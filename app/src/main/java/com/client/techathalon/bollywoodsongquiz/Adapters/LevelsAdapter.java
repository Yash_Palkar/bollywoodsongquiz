package com.client.techathalon.bollywoodsongquiz.Adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.client.techathalon.bollywoodsongquiz.DatabaseOperations.DatabaseHelper;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Model;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Questions;
import com.client.techathalon.bollywoodsongquiz.QuizQuestions;
import com.client.techathalon.bollywoodsongquiz.R;
import com.client.techathalon.bollywoodsongquiz.SharedPrefConfig;

import java.util.ArrayList;
import java.util.List;

public class LevelsAdapter extends PagerAdapter {

    private List<Model> models;
    private LayoutInflater layoutInflater;
    private Context context;
    ArrayList<Questions> mList;
    ArrayList<Integer> levelLockList;
    DatabaseHelper db;
    TextView score, count, lock;
    ImageView lockIcon;
    Button play;
    ProgressBar progressBar;
    CardView cardView;
    SharedPrefConfig prefConfig;
    TextView progresText, txt_unlock;
//    View view;
    LinearLayout lin_level;
    int level_lock;


    public LevelsAdapter(List<Model> models, Context context) {
        this.models = models;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        final View itemGroup = layoutInflater.inflate(R.layout.recycler_layout, container, false);

//        view = layoutInflater.inflate(R.layout.recycler_layout, container, false);
        db = new DatabaseHelper(context);
        prefConfig = new SharedPrefConfig(context);
        final int newPos = position + 1;
        count = itemGroup.findViewById(R.id.level_count);
        level_lock = prefConfig.readLevelLocked(position);
        lockIcon = itemGroup.findViewById(R.id.lockIcon);
        score = itemGroup.findViewById(R.id.score);
        lock = itemGroup.findViewById(R.id.levelLocked);
        play = itemGroup.findViewById(R.id.play);
        progressBar = itemGroup.findViewById(R.id.customProgress);
        cardView = itemGroup.findViewById(R.id.cardView);
        progresText = itemGroup.findViewById(R.id.myTextProgress);
        lin_level = itemGroup.findViewById(R.id.lin_level);
        txt_unlock = itemGroup.findViewById(R.id.txt_unlock);

        if (level_lock == 1) {
            lock.setText("Unlocked");
            lockIcon.setImageResource(R.drawable.lock_open_green);
            txt_unlock.setVisibility(View.VISIBLE);
            if (db.getRightCount(position + 1) > 55)
                txt_unlock.setText("Congrats! You had unlock the Level ");
            else
                txt_unlock.setText((55 - db.getRightCount(position + 1)) + " questions need to be solve to unlock next level");
        } else {
            lockIcon.setImageResource(R.drawable.lock_open);
            txt_unlock.setVisibility(View.VISIBLE);
        }

        getData(position);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, QuizQuestions.class);
                intent.putExtra("LevelCount", (newPos));
                v.getContext().startActivity(intent);
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, QuizQuestions.class);
                intent.putExtra("LevelCount", (newPos));
                v.getContext().startActivity(intent);
            }
        });
        container.addView(itemGroup);
        return itemGroup;
    }



    private void getData(int position) {
        int score_count = db.getScore(position + 1);
        ArrayList<Questions> guessedWrong = db.getGuessedAnswerList(position + 1);
        int realProgress = db.getGuessedRight(position + 1);
        int progress = (realProgress + guessedWrong.size());

        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", 0, progress);
        progressAnimator.setDuration(1500);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.start();
        score.setText("");
        count.setText(models.get(position).getLevel_count());

        score.setText("" + score_count);
        progressBar.setProgress((progress + guessedWrong.size()));
        progressBar.setSecondaryProgress(guessedWrong.size());
        progresText.setText(realProgress + " / 60");
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
