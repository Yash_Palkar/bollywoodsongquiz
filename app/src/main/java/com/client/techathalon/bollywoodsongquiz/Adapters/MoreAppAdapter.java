package com.client.techathalon.bollywoodsongquiz.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.client.techathalon.bollywoodsongquiz.ModelClass.MoreAppsModel;
import com.client.techathalon.bollywoodsongquiz.R;

import java.util.List;

public class MoreAppAdapter extends PagerAdapter {

    private List<MoreAppsModel> moreAppsList;
    private Context context;

    public MoreAppAdapter(List<MoreAppsModel> moreAppsList, Context context) {
        this.moreAppsList = moreAppsList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return moreAppsList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.more_app_layout, container, false);

        TextView appName = view.findViewById(R.id.app_title);
        ImageView appIcon = view.findViewById(R.id.app_icon);
        Button installNow = view.findViewById(R.id.install_now);

        appName.setText(moreAppsList.get(position).getApp_name());
        appIcon.setImageResource(moreAppsList.get(position).getApp_logo());

        final String app_Link = moreAppsList.get(position).getApp_url();

        installNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse(app_Link));
                v.getContext().startActivity(intent);
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
