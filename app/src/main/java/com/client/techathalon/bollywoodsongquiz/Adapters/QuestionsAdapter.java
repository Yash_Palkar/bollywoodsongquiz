package com.client.techathalon.bollywoodsongquiz.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.client.techathalon.bollywoodsongquiz.DatabaseOperations.DatabaseHelper;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Questions;
import com.client.techathalon.bollywoodsongquiz.QuizActivity;
import com.client.techathalon.bollywoodsongquiz.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionHolder> {

    private Context context;
    private ArrayList<Questions> mData;
    private ShareDialog shareDialog;
    private Dialog shareOnFb;
    private int score_count;
    private DatabaseHelper db;
    private int level;

    Activity activity = (Activity) context;

    public QuestionsAdapter(Context context, ArrayList<Questions> mData, ArrayList<Questions> wrongAnsweredList) {
        this.context = context;
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public QuestionsAdapter.QuestionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.quiz_questions, viewGroup, false);

        return new QuestionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionHolder questionHolder, final int position) {
        final int question_number = position + 1;
        int is_played = mData.get(position).getIs_played();
        int guessed = mData.get(position).getGuessed_right();
        final int level_unlocked = mData.get(position).getLevel_unlocked();
        questionHolder.number.setText(String.valueOf(question_number));
        if (level_unlocked == 0 && is_played == 0 && guessed == 0) {
            questionHolder.question.setVisibility(View.GONE);
            questionHolder.correct.setVisibility(View.GONE);
            questionHolder.wrong.setVisibility(View.GONE);
            questionHolder.lock.setVisibility(View.VISIBLE);
        } else if (level_unlocked == 1 && is_played == 0 && guessed == 0) {
            questionHolder.question.setVisibility(View.VISIBLE);
            questionHolder.correct.setVisibility(View.GONE);
            questionHolder.wrong.setVisibility(View.GONE);
            questionHolder.lock.setVisibility(View.GONE);
        } else if (level_unlocked == 1 && is_played == 1 && guessed == 0) {
            questionHolder.question.setVisibility(View.GONE);
            questionHolder.correct.setVisibility(View.GONE);
            questionHolder.wrong.setVisibility(View.VISIBLE);
            questionHolder.lock.setVisibility(View.GONE);
        } else if (level_unlocked == 1 && is_played == 1 && guessed == 1) {
            questionHolder.question.setVisibility(View.GONE);
            questionHolder.correct.setVisibility(View.VISIBLE);
            questionHolder.wrong.setVisibility(View.GONE);
            questionHolder.lock.setVisibility(View.GONE); }

        questionHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (level_unlocked == 0) {
                    Toast.makeText(context, "This Level is Locked", Toast.LENGTH_SHORT).show();
                } else {
//                    Intent intent = new Intent(context, QuizPlay.class);
                    Intent intent = new Intent(context, QuizActivity.class);
                    intent.putExtra("LevelCount", mData.get(position).getLevel());
                    intent.putExtra("QuestionNumber", question_number);
                    intent.putExtra("Position", position);
                    intent.putExtra("LIST", mData);
                    intent.putExtra("from", "");

//                    intent.putExtra("SongName", mData.get(position).getSong_name());
//                    intent.putExtra("MovieName", mData.get(position).getMovie_name());
//TODO                    intent.putExtra("from", from_where);
                    intent.putExtra("Position", position);
//                    intent.putExtra("tab_value", tab_value);
                    intent.putExtra("Level", mData.get(position).getLevel());
                    intent.putExtra("GuessedRight", mData.get(position).getGuessed_right());
                    intent.putExtra("WrongCount", mData.get(position).getWrongSelectionCount());
                    v.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class QuestionHolder extends RecyclerView.ViewHolder {

        ImageView lock, question, correct, wrong;
        TextView number;
        QuestionHolder(@NonNull View itemView) {
            super(itemView);
            db = new DatabaseHelper(context);
            correct = itemView.findViewById(R.id.correct);
            wrong = itemView.findViewById(R.id.wrong);
            lock = itemView.findViewById(R.id.lock);
            question = itemView.findViewById(R.id.question);
            number = itemView.findViewById(R.id.number);

            shareOnFb = new Dialog(itemView.getContext());
            shareDialog = new ShareDialog((Activity) itemView.getContext());
        }
    }
//
//    public void updateList(ArrayList<Questions> itemList){
//        list.clear();
////        this.list = itemList;
////        list.addAll(itemList);
////        notifyItemInserted(list.size());
////        notifyDataSetChanged();
//        this.list.addAll(itemList);
////        notifyItemInserted(list.size());
//        notifyDataSetChanged();
//    }

    public void showPopup() {
        shareOnFb.setContentView(R.layout.wrong_answer_popup);
        TextView dialogScore = shareOnFb.findViewById(R.id.totalScore);
        Button button = shareOnFb.findViewById(R.id.shareFacebook2);
        ImageView close = shareOnFb.findViewById(R.id.close);
        TextView incorrectAnswer = shareOnFb.findViewById(R.id.incorrectAnswer);

        score_count = db.getScore(level);
        dialogScore.setText(String.valueOf(score_count));
        incorrectAnswer.setText("25 Incorrect Answers");

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareOnFb.dismiss();
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareOnFacebook();
            }
        });

        shareOnFb.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shareOnFb.show();

    }


    public void shareOnFacebook() {
        ShareLinkContent linkContent = new ShareLinkContent.Builder().setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=allinonegame.techathalon.com.smashballhit&showAllReviews=true"))
                .setQuote("Highscore = " + score_count)
                .build();
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(linkContent);
        }
        CallbackManager callbackManager = CallbackManager.Factory.create();
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        boolean sharedOnFb = true;
    }
}
