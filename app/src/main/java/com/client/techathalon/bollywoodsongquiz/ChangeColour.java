package com.client.techathalon.bollywoodsongquiz;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.ImageView;

public class ChangeColour {
    Context context;
    ImageView image;
    Resources res;
    int newColor;

    ChangeColour(Context context, ImageView image) {
        this.context = context;
        this.image = image;
    }

    void setColor(int color) {
        res = context.getResources();
        newColor = res.getColor(color);
        image.setColorFilter(newColor, PorterDuff.Mode.SRC_ATOP);
    }
}
