package com.client.techathalon.bollywoodsongquiz.DatabaseOperations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.client.techathalon.bollywoodsongquiz.ModelClass.Questions;
import com.client.techathalon.bollywoodsongquiz.SharedPrefConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "QUIZ";
    private static final String TABLE_NAME = "BOLLYWOOD_SONGS";
    private static final String SONG_NAME = "SONG_NAME";
    private static final String MOVIE_NAME = "MOVIE_NAME";
    private static final String IS_PLAYED = "IS_PLAYED";
    private static final String SCORES = "SCORES";
    private static final String LEVEL = "LEVEL";
    private static final String GUESSED_RIGHT_WRONG = "GUESSED_RIGHT_WRONG";
    private static final String LEVEL_UNLOCKED = "LEVEL_UNLOCKED";
    private static final String WRONG_COUNT = "WRONG_COUNT";

    private static final String DATABASE_ALTER = "ALTER TABLE "
            + TABLE_NAME + " ADD COLUMN " + WRONG_COUNT + " INTEGER;";

    private String create = "create table " + TABLE_NAME + " (Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "SONG_NAME TEXT, MOVIE_NAME TEXT, IS_PLAYED INTEGER, " +
            "SCORES INTEGER, LEVEL INTEGER, GUESSED_RIGHT_WRONG INTEGER, LEVEL_UNLOCKED INTEGER, WRONG_COUNT INTEGER)";

    private Context context;
    SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 5);
        this.context = context;
        db = context.openOrCreateDatabase(DATABASE_NAME,
                Context.MODE_PRIVATE, null);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        if (oldVersion < 3) {
            db.execSQL(DATABASE_ALTER);
            addWrongCount(db);
        } else if (oldVersion == 4) {
            addWrongCount(db);
        } else {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            db.execSQL(create);
        }

        Log.d("UPGRADE", "ONUPGRADE CALLED");

    }

    private void addWrongCount(SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(WRONG_COUNT, 0);
        db.update(TABLE_NAME, contentValues, null, null);
    }

    public Cursor getSongRecords(int level) {
        db = this.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_NAME + " Where LEVEL = ?";

//        TODO it will slow down data fetching
//        cursor.moveToFirst();
//        if (cursor != null && cursor.moveToFirst()) {
//            tableToString(db, TABLE_NAME);
//        }
        return db.rawQuery(select, new String[]{String.valueOf(level)});
    }

    public ArrayList<String> getMovieOptions(String movieName) {
//        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT " + MOVIE_NAME +
//                " FROM " + TABLE_NAME + " ORDER BY RANDOM() LIMIT 3", null);

        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT " + MOVIE_NAME +
                " FROM (SELECT MOVIE_NAME FROM " + TABLE_NAME + " EXCEPT SELECT MOVIE_NAME FROM " +
                TABLE_NAME + " WHERE MOVIE_NAME = ? ) ORDER BY RANDOM() LIMIT 3", new String[]{movieName});

        ArrayList<String> movies = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                movies.add(cursor.getString(cursor.getColumnIndex(MOVIE_NAME)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return movies;
    }

    public boolean setCorrect(String Song_Name, int value, int is_played, int wrongCount) {
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IS_PLAYED, is_played);
        contentValues.put(GUESSED_RIGHT_WRONG, value);
        contentValues.put(WRONG_COUNT, wrongCount);
        db.update(TABLE_NAME, contentValues, SONG_NAME + " = ?", new String[]{Song_Name});
        return true;
    }

    public int getGuessAnswer(String song_name) {
        db = this.getReadableDatabase();
        int guess = 0;
        String select = "SELECT * FROM " + TABLE_NAME + " WHERE SONG_NAME = ? ";
        String[] selectionArgs = {song_name};
        Cursor cursor = db.rawQuery(select, selectionArgs);
        cursor.moveToFirst();
        if (cursor != null && cursor.moveToFirst()) {
            guess = cursor.getInt(cursor.getColumnIndex(GUESSED_RIGHT_WRONG));
        }
        cursor.close();
        return guess;
    }

    public int getWrongCount(String song_name) {
        db = this.getReadableDatabase();
        int count = 0;
        String select = "SELECT * FROM " + TABLE_NAME + " WHERE SONG_NAME = ? ";
        String[] selectionArgs = {song_name};
        Cursor cursor = db.rawQuery(select, selectionArgs);
//        cursor.moveToFirst();
        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getInt(cursor.getColumnIndex(WRONG_COUNT));
        }
        cursor.close();
        //cursor = null;
//        db.close();
//        db = null;
        return count;
    }

    public int getRightCount(int level) {
        db = this.getReadableDatabase();
        int count = 0;
        String select = "SELECT * FROM " + TABLE_NAME + " WHERE " + GUESSED_RIGHT_WRONG + " = ? AND " + LEVEL + " = ? AND " + IS_PLAYED + " = ?";
        String[] selectionArgs = {"1", String.valueOf(level), "1"};
        Cursor cursor = db.rawQuery(select, selectionArgs);
//        cursor.moveToFirst();
        count = cursor.getCount();
        cursor.close();
        //cursor = null;
//        db.close();
//        db = null;
        return count;
    }

    public int getWrongofLevel(int level) {
        db = this.getReadableDatabase();
        int count = 0;
        String select = "SELECT * FROM " + TABLE_NAME + " WHERE " + GUESSED_RIGHT_WRONG + " = ? AND " + LEVEL + " = ? AND " + IS_PLAYED + " = ?";
        String[] selectionArgs = {"0", String.valueOf(level), "1"};
        Cursor cursor = db.rawQuery(select, selectionArgs);
//        cursor.moveToFirst();
        count = cursor.getCount();
        cursor.close();
        //cursor = null;
//        db.close();
//        db = null;
        return count;
    }

    public int getIsplayed() {
        db = this.getReadableDatabase();
        int count = 0;
        String select = "SELECT * FROM " + TABLE_NAME + " WHERE IS_PLAYED= ? ";
        String[] selectionArgs = {"1"};
        Cursor cursor = db.rawQuery(select, selectionArgs);
//        cursor.moveToFirst();
        count = cursor.getCount();
        cursor.close();
        //cursor = null;
//        db.close();
//        db = null;
        return count;
    }

    public void setScore(int level, int score) {
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SCORES, score);
        db.update(TABLE_NAME, contentValues, LEVEL + " = ?", new String[]{String.valueOf(level)});
    }

    public int getScore(int level) {
        db = this.getReadableDatabase();
        int score = 0;
        String select = "SELECT " + SCORES + " FROM " + TABLE_NAME + " WHERE LEVEL = " + level;
        Cursor cursor = db.rawQuery(select, null);
        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    score = cursor.getInt(cursor.getColumnIndex(SCORES));
                }
            }
        } catch (IllegalStateException e) {
            Log.d("TAG", "Exception : " + e);
        } catch (Throwable throwable) {
            Log.d("TAG", "Exception : " + throwable);
        }
        if (cursor != null) {
            cursor.close();
        }
        return score;
    }

    public void unlockLevel(int level) {
        SQLiteDatabase db = getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LEVEL_UNLOCKED, 1);
        db.update(TABLE_NAME, contentValues, "LEVEL = ?", new String[]{String.valueOf(level)});
    }

//    public String tableToString(SQLiteDatabase db, String tableName) {
//        String tableString = String.format("Table %s:\n", tableName);
//        Cursor allRows = db.rawQuery("SELECT * FROM " + tableName, null);
//        tableString += cursorToString(allRows);
//        return tableString;
//    }

//    public String cursorToString(Cursor cursor) {
//        String cursorString = "";
//        if (cursor.moveToFirst()) {
//            String[] columnNames = cursor.getColumnNames();
//            for (String name : columnNames)
//                cursorString += String.format("[ %s ] ", name);
//            cursorString += "\n";
//            do {
//                for (String name : columnNames) {
//                    cursorString += String.format("[ %s ] ",
//                            cursor.getString(cursor.getColumnIndex(name)));
//                }
//                cursorString += "\n";
//            } while (cursor.moveToNext());
//        }
//        return cursorString;
//    }

    public ArrayList<Questions> getGuessedAnswerList(int level) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Questions> wrongGuessedList = new ArrayList<>();
        String select = "SELECT * FROM " + TABLE_NAME + " WHERE " + GUESSED_RIGHT_WRONG + " = 0 AND " + IS_PLAYED + " = 1";
        Cursor cursor = db.rawQuery(select, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(0);
                String song_name = cursor.getString(1);
                String movie_name = cursor.getString(2);
                int is_played = cursor.getInt(3);
                int scores = cursor.getInt(4);
                int level_count = cursor.getInt(5);
                int guessed_right = cursor.getInt(6);
                int level_unlocked = cursor.getInt(7);
                int wrong_count = cursor.getInt(8);

                if (level_count == level) {
                    Questions questions = new Questions(id, song_name, movie_name,
                            is_played, level_count, guessed_right, level_unlocked, wrong_count);
                    wrongGuessedList.add(questions);
                }
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return wrongGuessedList;
    }

    public int getGuessedRight(int level) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Questions> wrongGuessedList = new ArrayList<>();
        String select = "SELECT * FROM " + TABLE_NAME + " WHERE " + GUESSED_RIGHT_WRONG + " = 1 AND " + IS_PLAYED + " = 1";
        Cursor cursor = db.rawQuery(select, null);
        cursor.moveToFirst();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String song_name = cursor.getString(1);
                String movie_name = cursor.getString(2);
                int is_played = cursor.getInt(3);
                int scores = cursor.getInt(4);
                int level_count = cursor.getInt(5);
                int guessed_right = cursor.getInt(6);
                int level_unlocked = cursor.getInt(7);
                int wrong_count = cursor.getInt(8);

                if (level_count == level) {
                    Questions questions = new Questions(id, song_name, movie_name, is_played,
                            level_count, guessed_right, level_unlocked, wrong_count);
                    wrongGuessedList.add(questions);
                }
            } while (cursor.moveToNext());
            cursor.close();
        }

        return wrongGuessedList.size();
    }

    public boolean deleteSongs() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        return true;
    }

    public void initDatabaseData() {
        SQLiteDatabase db = getWritableDatabase();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(context.getResources().getAssets().open("BollywoodSongQuiz.txt")), 1024 * 4);
            String line = null;
            db.beginTransaction();
            while ((line = br.readLine()) != null) {
                line.trim();
                db.execSQL(line);
            }

            db.setTransactionSuccessful();
        } catch (IOException e) {
            Log.e("Database", "read database init file error");
        } finally {
            db.endTransaction();
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Log.e("Database", "buffer reader close error");
                }
            }
        }

        SharedPrefConfig prefConfig = new SharedPrefConfig(context);
        prefConfig.writeFirstInstallStatus(false);
    }
}