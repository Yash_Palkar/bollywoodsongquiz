package com.client.techathalon.bollywoodsongquiz;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.client.techathalon.bollywoodsongquiz.DatabaseOperations.DatabaseHelper;
import com.client.techathalon.bollywoodsongquiz.HttpHelperClass.Constants;
import com.client.techathalon.bollywoodsongquiz.HttpHelperClass.HttpHelper;
import com.client.techathalon.bollywoodsongquiz.HttpHelperClass.HttpHelperActivity;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Questions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.core.CrashlyticsCore;

//import io.fabric.sdk.android.Fabric;

public class FacebookLoginActivity extends HttpHelperActivity {
    private CallbackManager callbackManager;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListner;
    String song = "", movie = "";
    int position, level, guessed_right, tab_number;
    ArrayList<Questions> songList = new ArrayList<>();
    HttpHelper httpHelper1;
    FirebaseUser user;
    String email;
    DatabaseHelper db;
    SharedPrefConfig prefConfig;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void setBackApiResponse1(String url, String object) {
        super.setBackApiResponse1(url, object);
        if (url.equals(Constants.LOGIN_FACEBOOK)) {
            if (object != null && !object.equals("")) {
                try {
                    JSONObject jsonObject = new JSONObject(object);
                    String success = jsonObject.getString("success");

                    if (success.equalsIgnoreCase("true")) {

//                        Toast.makeText(this, "Password sent successfully to "
//                                        + userDataPrefs.getString("registered_email", "") + ", check your inbox ",
//                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());

//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder()
//                        .disabled(BuildConfig.DEBUG)
//                        .build())
//                .build();
//        Fabric.with(this, crashlyticsKit);
//        // Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

//        Crashlytics.setString("ACTIVITY_NAME", "FacebookLoginActivity");
//        Crashlytics.log("FacebookLoginActivity");

        callbackManager = CallbackManager.Factory.create();
        if(LoginManager.getInstance()!=null){
            LoginManager.getInstance().logOut();
        }

        prefConfig = new SharedPrefConfig(this);
        db = new DatabaseHelper(this);
        Intent intent = getIntent();
//        song = intent.getStringExtra("SongName");
//        tab_number = intent.getExtras().getInt("tab_number");
//        movie = intent.getStringExtra("MovieName");
        position = intent.getExtras().getInt("Position");
        level = intent.getExtras().getInt("Level");
//        guessed_right = intent.getExtras().getInt("GuessedRight");
        songList = (ArrayList<Questions>) intent.getSerializableExtra("list");
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(FacebookLoginActivity.this, QuizPlay.class);
                Intent i = new Intent(FacebookLoginActivity.this, QuizActivity.class);
                prefConfig.setTabNumber(tab_number);
                i.putExtra("LevelCount", level);
                i.putExtra("Position", position);
                i.putExtra("from", "from_fbcancel");
                i.putExtra("LIST", songList);
                i.putExtra("live_from_get", "");
                startActivity(i);
                finish();
            }

            @Override
            public void onError(FacebookException error) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                    prefConfig.setTabNumber(tab_number);
                    LoginManager.getInstance().logInWithReadPermissions(FacebookLoginActivity.this, Arrays.asList("email"));
                }

            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth != null) {
                    user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        email = user.getEmail();
                    }
                }
            }
        };
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
//                    Log.w("TAG", "signInWithCredential", task.getException());
//                    Toast.makeText(FacebookLoginActivity.this, "Authentication failed.",
//                            Toast.LENGTH_SHORT).show();
                } else {
                    if (user != null) {
//                        Toast.makeText(FacebookLoginActivity.this, "Hello " + user.getEmail(), Toast.LENGTH_LONG).show();

                        if (email != null && !email.equalsIgnoreCase("")) {
                            HashMap<String, String> hashMap = new HashMap<String, String>();
                            hashMap.put("email_id", email);
                            hashMap.put("platform", "1");
                            httpHelper1 = new HttpHelper(Constants.LOGIN_FACEBOOK, hashMap, FacebookLoginActivity.this, "");
                        }
                    }else {
                        Toast.makeText(FacebookLoginActivity.this, "Cannot login with facebook id because of private settings", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(FacebookLoginActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                    prefConfig.setFirstTimefblogin(true);
//TODO                    Intent i = new Intent(FacebookLoginActivity.this, QuizPlay.class);
                    prefConfig.clearWrongCount();
                    Intent i = new Intent(FacebookLoginActivity.this, QuizActivity.class);
                    i.putExtra("LevelCount", level);
                    i.putExtra("Position", position);
                    i.putExtra("from", "fromfb");
                    i.putExtra("LIST", songList);
                    i.putExtra("live_from_get", "facebook_login");
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListner);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(firebaseAuthListner);
        if (db!=null){
            db.close();
        }
    }

}
