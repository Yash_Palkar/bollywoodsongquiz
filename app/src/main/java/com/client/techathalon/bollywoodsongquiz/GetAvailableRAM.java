package com.client.techathalon.bollywoodsongquiz;

import android.app.ActivityManager;
import android.content.Context;

import static android.content.Context.ACTIVITY_SERVICE;

public class GetAvailableRAM {

    public Long getRamMemory(Context context) {
        long availableMegs = 0;
        try {
            ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
            ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
            activityManager.getMemoryInfo(mi);
            availableMegs = mi.availMem / 1048576L;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return availableMegs;
    }

}
