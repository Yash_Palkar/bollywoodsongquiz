package com.client.techathalon.bollywoodsongquiz.HttpHelperClass;

import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;

import java.util.HashMap;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class HttpHelper {
    private ProgressDialog dialog = null;
    private String loadingMessage, url;
    private HashMap<String, String> map;
    private HttpHelperActivity activity_Main;
    ConnectivityManager connectivityManager;
    NetworkInfo networkInfo;
    JsonParser jParser;

    public HttpHelper() {
        //empty constructor
    }

    public HttpHelper(String url, HashMap<String, String> map, HttpHelperActivity activity, String loadingMessage) {
        this.url = url;
        this.map = map;
        activity_Main = activity;
        dialog = new ProgressDialog(activity_Main);
        this.loadingMessage = loadingMessage;
        if (isConnected(activity_Main))
            new JSONParse().execute();
        else
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity_Main.getApplicationContext(), "No Internet Connection",
                            Toast.LENGTH_LONG).show();
                    if (dialog != null && dialog.isShowing() && activity_Main != null && !activity_Main.isFinishing()) {
                        dialog.dismiss();
                    }
                }
            }, 1000);
    }

    private boolean isConnected(HttpHelperActivity activity_Main) {
        if (activity_Main != null) {
            connectivityManager = (ConnectivityManager) activity_Main.getSystemService(CONNECTIVITY_SERVICE);
            networkInfo = connectivityManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
        } else
            return true;
    }

    private class JSONParse extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialog != null) {
                dialog.setMessage(loadingMessage);
                if (dialog.isShowing())
                    dialog.dismiss();
                if (!url.equals(Constants.LOGIN_FACEBOOK)) {
                    if (activity_Main != null && !activity_Main.isFinishing())
                        dialog.show();
                }
            }
        }

        @Override
        protected String doInBackground(Void... voids) {
            jParser = new JsonParser();
            return jParser.performPostCall(url, map, activity_Main);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (dialog != null && dialog.isShowing() && activity_Main != null && !activity_Main.isFinishing())
                dialog.dismiss();
            if (!s.equals("") && activity_Main != null && !activity_Main.isFinishing())
                activity_Main.setBackApiResponse1(url, s);
            else
                activity_Main.setBackApiResponse1(url, "");
        }
    }
}
