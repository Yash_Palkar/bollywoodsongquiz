package com.client.techathalon.bollywoodsongquiz;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.client.techathalon.bollywoodsongquiz.Adapters.LevelsAdapter;
import com.client.techathalon.bollywoodsongquiz.DatabaseOperations.DatabaseHelper;
import com.client.techathalon.bollywoodsongquiz.ModelClass.CustomModel;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Model;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Questions;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CustomModel.OnCustomStateListener,
        View.OnClickListener, WSCallerVersionListener, RewardedVideoAdListener {

    AdView adView;
    ImageView howToPlay, share, close, setting, moreApps, closeSetting, img_getlive1, img1, img2, img3, img4, img5;
    List<Model> models;
    ViewPager viewPager;
    DatabaseHelper db;
    CardView rateUs, feedback, reset, moreApp;
    ArrayList<Questions> list;
    LevelsAdapter levelsAdapter;
    Integer[] colors;
    ArrayList<Integer> guessed;
    SpinKitView spinKitView;
    RelativeLayout transparentView;
    TextView score;
    int totalscore, pagerPosition = 4;
    RelativeLayout mainLayout;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    Dialog playDialog, settingDialog;
    SharedPrefConfig prefConfig;
    ShareDialog shareDialog;
    Dialog wrong_answers;
    CallbackManager callbackManager;
    int MY_REQUEST_CODE = 9999;
    Dialog livepopup;
    Button watchVideo, button;
    Long memory = 0L;
    GetAvailableRAM getAvailableRAM;
    boolean isForceUpdate = true;
    //    private FirebaseAnalytics mFirebaseAnalytics;
    AdRequest adRequest;
    InterstitialAd mInterstitialAd;
    TextView txt_live_msg;
    CheckInternet checkInternet;
    RewardedVideoAd mRewardedVideoAd1;
    static boolean videoWatched1 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
            } else {

                Log.v("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
        }
        checkInternet = new CheckInternet();
        getAvailableRAM = new GetAvailableRAM();
        mRewardedVideoAd1 = MobileAds.getRewardedVideoAdInstance(MainActivity.this);
        mRewardedVideoAd1.setRewardedVideoAdListener(MainActivity.this);
        loadRewardedVideoAd();
        loadInterstitialAd();
//        CrashlyticsCore core = new CrashlyticsCore.Builder().build();
//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder()
//                        .disabled(BuildConfig.DEBUG)
//                        .build())
//                .build();
//        Fabric.with(this, crashlyticsKit);
//        // Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

//        Crashlytics.setString("ACTIVITY_NAME", "Main Activity");
//        Crashlytics.log("Main Activity");
//

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        adView = findViewById(R.id.ads);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        wrong_answers = new Dialog(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        CustomModel.getInstance().setListener(this);
        prefConfig = new SharedPrefConfig(getApplicationContext());
        mainLayout = findViewById(R.id.main_layout);
        db = new DatabaseHelper(getApplicationContext());
        list = new ArrayList<>();
        guessed = new ArrayList<>();
        spinKitView = findViewById(R.id.spin_kit);
        transparentView = findViewById(R.id.transparentView);
        howToPlay = findViewById(R.id.howToPlay);
        score = findViewById(R.id.overallScore);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img5 = findViewById(R.id.img5);
        img_getlive1 = findViewById(R.id.img_getlive);

        share = findViewById(R.id.share);
        playDialog = new Dialog(MainActivity.this);
        settingDialog = new Dialog(MainActivity.this);
        prefConfig = new SharedPrefConfig(getApplicationContext());
        setting = findViewById(R.id.settings);
        moreApps = findViewById(R.id.more_apps);

        txt_live_msg = findViewById(R.id.txt_live_msg);
        viewPager = findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setPadding(75, 0, 75, 0);

        models = new ArrayList<>();
        models.add(new Model(getResources().getColor(R.color.color1), "Level 1"));
        models.add(new Model(getResources().getColor(R.color.color2), "Level 2"));
        models.add(new Model(getResources().getColor(R.color.color3), "Level 3"));
        models.add(new Model(getResources().getColor(R.color.color4), "Level 4"));
        models.add(new Model(getResources().getColor(R.color.color5), "Level 5"));
        models.add(new Model(getResources().getColor(R.color.color6), "Level 6"));
        models.add(new Model(getResources().getColor(R.color.color7), "Level 7"));

        colors = new Integer[]{
                getResources().getColor(R.color.color1),
                getResources().getColor(R.color.color2),
                getResources().getColor(R.color.color3),
                getResources().getColor(R.color.color4),
                getResources().getColor(R.color.color5),
                getResources().getColor(R.color.color6),
                getResources().getColor(R.color.color7)};
        setAdapter();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position < (levelsAdapter.getCount() - 1) && position < (models.size() - 1)) {
                    mainLayout.setBackgroundColor((Integer) argbEvaluator.evaluate(positionOffset,
                            colors[position],
                            colors[position + 1]));
                } else {
                    mainLayout.setBackgroundColor(colors[position]);
                }
            }

            @Override
            public void onPageSelected(int position) {
                prefConfig.setPagerPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        //   printHashKey();
        totalscore = prefConfig.getHighscore();
        score.setText("" + totalscore);
        share.setOnClickListener(this);
        howToPlay.setOnClickListener(this);
        setting.setOnClickListener(this);
        moreApps.setOnClickListener(this);
        img_getlive1.setOnClickListener(this);
        new PlayStoreAppVersionLoader(getApplicationContext(), this).execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (videoWatched1) {
            prefConfig.clearWrongCount();
            txt_live_msg.setVisibility(View.VISIBLE);
            Animation logoMoveAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
            txt_live_msg.startAnimation(logoMoveAnimation);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    txt_live_msg.setVisibility(View.GONE);
                }
            }, 3000);
//            setLives();
        }
        if (livepopup != null && livepopup.isShowing()) {
            livepopup.dismiss();
        }
        mRewardedVideoAd1 = MobileAds.getRewardedVideoAdInstance(MainActivity.this);
        mRewardedVideoAd1.setRewardedVideoAdListener(MainActivity.this);
        loadRewardedVideoAd();
//        setLives();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (db != null) {
            db.close();
        }
        if (adView != null){
            Log.d("Adview", "Adview Destroy");
            adView.destroy();
        }
    }

    public void setLives() {
        switch (prefConfig.getWrongCount()) {
            case 0: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live);
                img3.setImageResource(R.drawable.live);
                img4.setImageResource(R.drawable.live);
                img5.setImageResource(R.drawable.live);
            }
            break;
            case 1: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live);
                img3.setImageResource(R.drawable.live);
                img4.setImageResource(R.drawable.live);
                img5.setImageResource(R.drawable.live);
            }
            break;
            case 2: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live);
                img3.setImageResource(R.drawable.live);
                img4.setImageResource(R.drawable.live);
                img5.setImageResource(R.drawable.live_over);
            }
            break;
            case 3: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live);
                img3.setImageResource(R.drawable.live);
                img4.setImageResource(R.drawable.live_over);
                img5.setImageResource(R.drawable.live_over);
            }
            break;
            case 4: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live);
                img3.setImageResource(R.drawable.live_over);
                img4.setImageResource(R.drawable.live_over);
                img5.setImageResource(R.drawable.live_over);
            }
            break;
            case 5: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live_over);
                img3.setImageResource(R.drawable.live_over);
                img4.setImageResource(R.drawable.live_over);
                img5.setImageResource(R.drawable.live_over);
            }
            break;
            default: {
                img1.setImageResource(R.drawable.live_over);
                img2.setImageResource(R.drawable.live_over);
                img3.setImageResource(R.drawable.live_over);
                img4.setImageResource(R.drawable.live_over);
                img5.setImageResource(R.drawable.live_over);
            }

        }
    }

    public void setAdapter() {
        levelsAdapter = new LevelsAdapter(models, this);
        viewPager.setAdapter(levelsAdapter);
        viewPager.setCurrentItem(prefConfig.getPagerPosition());
    }

    private void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.client.techathalon.bollywoodsongquiz",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("HASHKEY", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stateChanged() {
        list = CustomModel.getInstance().getState();
        setAdapter();
        totalscore = prefConfig.getHighscore();
        score.setText("" + totalscore);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.howToPlay:
                showPopup();
                break;

            case R.id.share:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Download Bollywood Song Quiz - Guess the songs app at " + getResources().getString(R.string.app_link) + " and get to Guess the movie name of your favorite played song. Download Now!");
                shareIntent.setType("text/plain");
                startActivity(Intent.createChooser(shareIntent, "Share with..."));
                break;

            case R.id.more_apps:
                Intent in = new Intent(this, MoreApps.class);
                startActivity(in);
                break;

            case R.id.settings:
                showSetting();
                break;

            case R.id.close_bottomSheet:
                if (playDialog != null && playDialog.isShowing()) {
                    playDialog.dismiss();
                }
                break;

            case R.id.close_setting:
                if (settingDialog != null && settingDialog.isShowing()) {
                    settingDialog.dismiss();
                }
                break;

            case R.id.rate_us:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse(getResources().getString(R.string.app_link)));
                startActivity(intent);
                break;

            case R.id.img_getlive:
                showgetLivePopup();
                break;
            case R.id.watchVideo:
                watchVideo();
                break;
            case R.id.shareOnFacebook:
                if (checkInternet.isConnectedToInternet()) {
                    shareOnFb();
                } else {
                    Toast.makeText(MainActivity.this, "Check Your internet connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.feedback:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@techathalon.com"});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback / Suggestion for Bollywood Song Quiz - Android App");
                emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
                break;

//            case R.id.like_us:
//                try {
//                    getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.facebook.com/HBollywood-483538345072706/")));
//                } catch (Exception e) {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.facebook.com/HBollywood-483538345072706/")));
//                }
//                break;

            case R.id.reset:
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Are You Sure?")
                        .setMessage("It will reset your score and it can not be undone")
                        .setCancelable(false)
                        .setPositiveButton("Reset Now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                prefConfig.clearPreference();
                                db.deleteSongs();
                                db.initDatabaseData();
                                Toast.makeText(getApplicationContext(), "App Reset Successfull", Toast.LENGTH_SHORT).show();
                                stateChanged();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void shareOnFb() {
        ShareLinkContent linkContent = new ShareLinkContent.Builder().
                setContentUrl(Uri.parse(getResources().getString(R.string.app_link)))
                .setQuote("Download the app")
                .build();

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(linkContent);
        }

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                if (livepopup != null && livepopup.isShowing()) {
                    livepopup.dismiss();
                }
                prefConfig.clearWrongCount();
                txt_live_msg.setVisibility(View.VISIBLE);
                Animation logoMoveAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
                txt_live_msg.startAnimation(logoMoveAnimation);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txt_live_msg.setVisibility(View.GONE);
                    }
                }, 3000);
//                setLives();
            }


            @Override
            public void onCancel() {
//                prefConfig.setTabNumber(viewpager.getCurrentItem() + 1);
                if (livepopup != null && livepopup.isShowing()) {
                    livepopup.dismiss();
                }

                Toast.makeText(MainActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
//                prefConfig.setTabNumber(viewpager.getCurrentItem() + 1);
                error.printStackTrace();
            }
        });
    }

    public void watchVideo() {
        memory = getAvailableRAM.getRamMemory(MainActivity.this);
        if (checkInternet.isConnectedToInternet()) {
            try {
                if (memory >= 400) {
                    if (mRewardedVideoAd1 != null && mRewardedVideoAd1.isLoaded()) {
                        mRewardedVideoAd1.show();
                    } else {
                        if (!isFinishing()) {
                            Toast.makeText(MainActivity.this, "Waiting for this ad to load. Please wait or click again...", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Some Error Occurred", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(MainActivity.this, "Some Error Occurred", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MainActivity.this, "Check Your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void showPopup() {
        playDialog.setContentView(R.layout.bottom_sheet_layout);
        close = playDialog.findViewById(R.id.close_bottomSheet);
        close.setOnClickListener(this);
        playDialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.MATCH_PARENT;
        playDialog.getWindow().getAttributes().height = ViewGroup.LayoutParams.MATCH_PARENT;
        playDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        playDialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
        playDialog.show();
    }

    public void showgetLivePopup() {
        livepopup = new Dialog(MainActivity.this);
        livepopup.setContentView(R.layout.dialog_getlives);

        button = livepopup.findViewById(R.id.shareOnFacebook);
        watchVideo = livepopup.findViewById(R.id.watchVideo);
        close = livepopup.findViewById(R.id.close);
        TextView title = livepopup.findViewById(R.id.incorrectAnswer);

        memory = getAvailableRAM.getRamMemory(MainActivity.this);
        if (memory < 400) {
            watchVideo.setClickable(false);
            watchVideo.setVisibility(View.GONE);
        } else {
            watchVideo.setClickable(true);
            watchVideo.setVisibility(View.VISIBLE);
        }

//        if (prefConfig.getWrongCount() >= 6) {
//            title.setText("5 Attempts Completed!");
//            attempts.setVisibility(View.VISIBLE);
//            attempts.setText("Select one to Unlock...");
//            lin_share.setVisibility(View.VISIBLE);
//        } else {
//            lin_share.setVisibility(View.GONE);
//            attempts.setText((5 - prefConfig.getWrongCount()) + " Attempts Remaining!");
//            prefConfig.setWrongCount(1);
//        }
//

        button.setOnClickListener(this);
        watchVideo.setOnClickListener(this);
        livepopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        if (!isFinishing()) {
        livepopup.show();
//        }
        livepopup.setCancelable(false);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                livepopup.dismiss();


            }
        });
    }

    public void showSetting() {

        settingDialog.setContentView(R.layout.app_setting_layout);
        closeSetting = settingDialog.findViewById(R.id.close_setting);
        rateUs = settingDialog.findViewById(R.id.rate_us);
        feedback = settingDialog.findViewById(R.id.feedback);
        reset = settingDialog.findViewById(R.id.reset);
        // likeUs = (CardView) settingDialog.findViewById(R.id.like_us);

        closeSetting.setOnClickListener(this);
        rateUs.setOnClickListener(this);
        feedback.setOnClickListener(this);
        reset.setOnClickListener(this);
//      likeUs.setOnClickListener(this);

        settingDialog.getWindow().getAttributes().width = android.view.ViewGroup.LayoutParams.MATCH_PARENT;
        settingDialog.getWindow().getAttributes().height = ViewGroup.LayoutParams.MATCH_PARENT;
        settingDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        settingDialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
        settingDialog.show();
    }

    @Override
    public void onGetResponse(boolean isUpdateAvailable) {
        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
        if (isUpdateAvailable) {
            if (!isFinishing())
                showUpdateDialog();
        }
    }

    public void showUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setTitle(MainActivity.this.getString(R.string.app_name));
        alertDialogBuilder.setMessage("Application Update is Available");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MainActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isForceUpdate = false;
//                if (isForceUpdate && isFinishing()) {
//                    finish();
//                }
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }

    private void loadRewardedVideoAd() {
        try {
            if (mRewardedVideoAd1 != null) {
                mRewardedVideoAd1.loadAd(getResources().getString(R.string.rewared_video_ad),
                        new AdRequest.Builder().build());
            }
        } catch (Exception e) {
            Log.d("EXCEPTION:", "" + e);
        }
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.d("TAG", "onRewardedVideoAdLoaded1");
        videoWatched1 = false;
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.d("TAG", "onRewardedVideoAdOpened1");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.d("TAG", "onRewardedVideoStarted1");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.d("TAG", "onRewardedVideoAdClosed1");
        loadRewardedVideoAd();
        if (livepopup != null && livepopup.isShowing()) {
            livepopup.dismiss();
        }
//        setLives();
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.d("TAG", "onRewarded1");
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.d("TAG", "onRewardedVideoAdLeftApplication1");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        loadRewardedVideoAd();
        Log.d("TAG", "onRewardedVideoAdFailedToLoad1");
    }

    @Override
    public void onRewardedVideoCompleted() {
        Log.d("TAG", "onRewardedVideoCompleted1");
        if (mRewardedVideoAd1 != null) {
            mRewardedVideoAd1.destroy(MainActivity.this);
        }
//        if (livepopup != null && livepopup.isShowing()) {
//            livepopup.dismiss();
//        }
        videoWatched1 = true;

        prefConfig.clearWrongCount();
        txt_live_msg.setVisibility(View.VISIBLE);
        Animation logoMoveAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
        txt_live_msg.startAnimation(logoMoveAnimation);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                txt_live_msg.setVisibility(View.GONE);
            }
        }, 3000);
//        setLives();
        mRewardedVideoAd1 = MobileAds.getRewardedVideoAdInstance(MainActivity.this);
        mRewardedVideoAd1.setRewardedVideoAdListener(MainActivity.this);
        loadRewardedVideoAd();
    }

    private void loadInterstitialAd() {
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId(getApplicationContext().getResources().getString(R.string.interstitial_ad));
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }


}