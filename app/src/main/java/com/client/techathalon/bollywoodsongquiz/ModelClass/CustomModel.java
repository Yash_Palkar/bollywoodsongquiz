package com.client.techathalon.bollywoodsongquiz.ModelClass;

import java.util.ArrayList;

public class CustomModel {

    public interface OnCustomStateListener {
        void stateChanged();
    }

    private static CustomModel mInstance;
    private OnCustomStateListener mListener;
    private ArrayList<Questions> mList;

    private CustomModel() {}

    public static CustomModel getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModel();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeState(ArrayList<Questions> list) {
        if(mListener != null) {
            mList = list;
            notifyStateChange();
        }
    }

    public ArrayList<Questions> getState() {
        return mList;
    }

    private void notifyStateChange() {
        mListener.stateChanged();
    }
}