package com.client.techathalon.bollywoodsongquiz.ModelClass;

public class Model {

    private int color;
    private String level_count;

    public Model(int color, String level_count) {
        this.color = color;
        this.level_count = level_count;
    }

    public int getColor() {
        return color;
    }
    public void setColor(int color) {
        this.color = color;
    }
    public String getLevel_count() {
        return level_count;
    }

}
