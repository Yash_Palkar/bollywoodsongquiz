package com.client.techathalon.bollywoodsongquiz.ModelClass;

public class MoreAppsModel {

    private int app_logo;
    private String app_name;
    private String app_url;

    public MoreAppsModel(int app_logo, String app_name, String app_url) {
        this.app_logo = app_logo;
        this.app_name = app_name;
        this.app_url = app_url;
    }
    public int getApp_logo() {
        return app_logo;
    }
    public String getApp_name() {
        return app_name;
    }
    public String getApp_url() {
        return app_url;
    }
}
