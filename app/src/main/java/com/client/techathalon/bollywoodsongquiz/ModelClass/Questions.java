package com.client.techathalon.bollywoodsongquiz.ModelClass;

import java.io.Serializable;

public class Questions implements Serializable {

    private int id;
    private String song_name;
    private String movie_name;
    private int is_played;
    private int level;
    private int guessed_right;
    private int level_unlocked;
    private int wrongSelectionCount;

    public Questions(int id, String song_name, String movie_name, int is_played,
                     int level, int guessed_right, int level_unlocked, int wrongSelectionCount) {
        this.id = id;
        this.song_name = song_name;
        this.movie_name = movie_name;
        this.is_played = is_played;
        this.level = level;
        this.guessed_right = guessed_right;
        this.level_unlocked = level_unlocked;
        this.wrongSelectionCount = wrongSelectionCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSong_name() {
        return song_name;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public int getIs_played() {
        return is_played;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getGuessed_right() {
        return guessed_right;
    }

    public int getLevel_unlocked() {
        return level_unlocked;
    }

    public int getWrongSelectionCount() { return wrongSelectionCount; }

}














