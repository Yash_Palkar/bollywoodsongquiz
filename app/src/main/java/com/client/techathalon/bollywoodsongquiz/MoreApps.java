package com.client.techathalon.bollywoodsongquiz;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.client.techathalon.bollywoodsongquiz.Adapters.MoreAppAdapter;
import com.client.techathalon.bollywoodsongquiz.ModelClass.MoreAppsModel;

import java.util.ArrayList;
import java.util.List;

public class MoreApps extends AppCompatActivity {
    ViewPager viewPager;
    MoreAppAdapter moreAppAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_apps);

        viewPager = findViewById(R.id.moreAppsPager);
        viewPager.setPadding(75, 0, 75, 0);

        List<MoreAppsModel> moreAppsList = new ArrayList<>();

        moreAppsList.add(new MoreAppsModel(R.drawable.backup_app_icon, "My Contact Easy Backup Restore",
                "https://play.google.com/store/apps/details?id=com.client.backupcontact&hl=en"));

        moreAppsList.add(new MoreAppsModel(R.drawable.ball_hit_pro, "Ball Hits - Fire",
                "https://play.google.com/store/apps/details?id=allinonegame.techathalon.com.smashballhit&hl=en"));

        moreAppsList.add(new MoreAppsModel(R.drawable.scan_contact, "Smart Contact Manager Scan Contacts",
                "https://play.google.com/store/apps/details?id=com.client.scancontacts&hl=en"));

        moreAppsList.add(new MoreAppsModel(R.drawable.smart_contact_manager, "Smart Contact Manager",
                "https://play.google.com/store/apps/details?id=techathalon.com.smartcontactmanager&hl=en"));

        moreAppsList.add(new MoreAppsModel(R.drawable.smash_ball_hit, "Smash Balls Hit",
                "https://play.google.com/store/apps/details?id=com.techathalon.hitme&hl=en"));

        moreAppsList.add(new MoreAppsModel(R.drawable.water_reminder_logo, "Drink Water - Reminder & Tracker",
                "https://play.google.com/store/apps/details?id=com.client.watertracker"));

        moreAppsList.add(new MoreAppsModel(R.drawable.pedometer_logo,
                "Pedometer",
                "https://play.google.com/store/apps/details?id=com.client.pedometer"));

        moreAppsList.add(new MoreAppsModel(R.drawable.new_final1,
                        "Bollywood Movie Quiz Game - Guess the Movie",
                "https://play.google.com/store/apps/details?id=com.client.Boxofficequiz"));

        moreAppsList.add(new MoreAppsModel(R.drawable.logo,
                        "Bollywood Movie Quiz - Guess the movie",
                "https://play.google.com/store/apps/details?id=techathalon.bollywoodmoviequiz"));

        moreAppsList.add(new MoreAppsModel(R.drawable.finallogo,
                        "Birthday Reminder & Smart Contact Manager Scan",
                "https://play.google.com/store/apps/details?id=com.client.birthday_contact"));

        moreAppsList.add(new MoreAppsModel(R.drawable.app_logo_new,
                        "Bollywood Hollywood Movies Quiz -Guess the stars",
                "https://play.google.com/store/apps/details?id=com.client.hbollyoodstarquiz"));

        moreAppsList.add(new MoreAppsModel(R.drawable.hnplogo,
                                "Smart Housie Number Picker",
                "https://play.google.com/store/apps/details?id=com.client.kushthakkar.housienumberpicker"));


        moreAppAdapter = new MoreAppAdapter(moreAppsList, getApplicationContext());
        viewPager.setAdapter(moreAppAdapter);

    }
}
