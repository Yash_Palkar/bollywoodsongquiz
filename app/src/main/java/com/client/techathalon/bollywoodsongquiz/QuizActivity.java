package com.client.techathalon.bollywoodsongquiz;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.client.techathalon.bollywoodsongquiz.DatabaseOperations.DatabaseHelper;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Questions;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener, RewardedVideoAdListener {

    private static final int COUNTDOWN_IN_MILLI = 20000;
    static boolean videoWatched = false;
    static boolean isPaused = false;
    private static long TIME_LEFT;
    ImageView close, img_incorrect;
    TextView score, timer, previousScore, dialogScore, attempts, quizNumber, dialogScore1, txt_desc;
    LinearLayout lin_share, lin_coins, lin_live_main;
    Button pause, play, opt2, opt3, opt4, opt1, resume, exit, back_game, starButton, button, watchVideo;
    ProgressBar timerProgress;
    Dialog wrongAnswer, pauseDialog, earn_dialog;
    InterstitialAd mInterstitialAd;
    ArrayList<String> optionsList;
    List<Questions> wrongGuessedList;
    int level, position, scoreCount = 0, mediaCurrentPosition, guessedRightFlag, wrongCount, highscore;
    String song, movie, from_where, currentSong = "";
    MediaPlayer mediaPlayer = null;
    DatabaseHelper db;
    ArrayList<Questions> quizList = new ArrayList<>();
    String[] movieNameArray;
    Intent intent;
    String get_live_from = "", live_from_get = "";
    boolean stopFlag = false;
    Animation animation, left_to_right, right_to_left;
    RotateAnimation rotate;
    Uri uri;
    RewardedVideoAd mRewardedVideoAd;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    Long memory = 0L;
    SharedPrefConfig prefConfig;
    GetAvailableRAM getAvailableRAM;
    CheckInternet checkInternet;
    ImageView img1, img2, img3;
    Dialog level_completed;
    ProgressBar progressBar;
    TextView txt_live_msg, coin_add, progresText, title;
    RelativeLayout rel_main;
    private CountDownTimer countDownTimer;
    private ImageView image1;
    private int[] imageArray;
    private int currentIndex;
    private int startIndex;
    private int endIndex;
    AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_activty);
        MobileAds.initialize(this,
                getResources().getString(R.string.banner_ad));
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView = findViewById(R.id.quiz_activity_adview);
        mAdView.loadAd(adRequest);

        db = new DatabaseHelper(getApplicationContext());

        prefConfig = new SharedPrefConfig(QuizActivity.this);
        getAvailableRAM = new GetAvailableRAM();
        checkInternet = new CheckInternet();
        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();
        level_completed = new Dialog(QuizActivity.this);
        progressBar = findViewById(R.id.customProgress);
        progresText = findViewById(R.id.myTextProgress);
        txt_live_msg = findViewById(R.id.txt_live_msg);
        image1 = findViewById(R.id.slide_trans_imageswitcher);
        new ChangeColour(QuizActivity.this, image1).setColor(R.color.image_icon);
        imageArray = new int[16];
        imageArray[0] = R.drawable.wave_1;
        imageArray[1] = R.drawable.wave_2;
        imageArray[2] = R.drawable.wave_3;
        imageArray[3] = R.drawable.wave_4;
        imageArray[4] = R.drawable.wave_5;
        imageArray[5] = R.drawable.wave_6;
        imageArray[6] = R.drawable.wave_7;
        imageArray[7] = R.drawable.wave_8;
        imageArray[8] = R.drawable.wave_9;
        imageArray[9] = R.drawable.wave_10;
        imageArray[10] = R.drawable.wave_11;
        imageArray[11] = R.drawable.wave_12;
        imageArray[12] = R.drawable.wave_13;
        imageArray[13] = R.drawable.wave_14;
        imageArray[14] = R.drawable.wave_15;
        imageArray[15] = R.drawable.wave_16;

        startIndex = 0;
        endIndex = 15;
        nextImage();
        intent = getIntent();
        quizList = new ArrayList<>();
        quizList = (ArrayList<Questions>) intent.getSerializableExtra("LIST");
        level = intent.getIntExtra("LevelCount", 0);
        position = intent.getIntExtra("Position", 0);
        from_where = intent.getStringExtra("from");
        wrongCount = intent.getIntExtra("WrongCount", 0);
        if (intent.getStringExtra("live_from_get") != null)
            live_from_get = intent.getStringExtra("live_from_get");
        else
            live_from_get = "";

        wrongAnswer = new Dialog(QuizActivity.this);
        pauseDialog = new Dialog(QuizActivity.this);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });


        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(QuizActivity.this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();
        optionsList = new ArrayList<>();
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
//        img4 = findViewById(R.id.img4);
//        img5 = findViewById(R.id.img5);
        score = findViewById(R.id.score_count);
        timer = findViewById(R.id.timer_count);
        opt1 = findViewById(R.id.answer1);
        opt2 = findViewById(R.id.answer2);
        opt3 = findViewById(R.id.answer3);
        opt4 = findViewById(R.id.answer4);
        play = findViewById(R.id.music_player);
        pause = findViewById(R.id.music_pause);
        coin_add = findViewById(R.id.coin_add);
//        coin_add.setText("50");
        setLives();
        quizNumber = findViewById(R.id.quiz_number);
        timerProgress = findViewById(R.id.progress_bar);
        starButton = findViewById(R.id.star_button);

        play.setVisibility(View.GONE);
        opt1.setOnClickListener(this);
        opt2.setOnClickListener(this);
        opt3.setOnClickListener(this);
        opt4.setOnClickListener(this);
        pause.setOnClickListener(this);
        play.setOnClickListener(this);
        play.setClickable(false);
        pause.setClickable(false);
        wrongGuessedList = db.getGuessedAnswerList(level);
        if (wrongGuessedList.size() >= 25) {
            if (!prefConfig.readWrongShareOnFb(level)) {
                finish();
            }
        }
        initView();
        getData(position);
        initQuiz(position);
        if (!live_from_get.equals("") && live_from_get.equals("facebook_login")) {
            if (!currentSong.equals("")) {
                prefConfig.clearWrongCount();
                setLives();
                db.setCorrect(currentSong, 0, 1, 0);
            }
            Toast.makeText(QuizActivity.this, "Congratulations!! You Earn 3 lives.. Continue playing to earn more!!!",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopMusic();
    }

    public void nextImage() {
        image1.setImageResource(imageArray[currentIndex]);
        currentIndex++;
        new Handler().postDelayed(() -> {
            if (currentIndex > endIndex) {
                currentIndex--;
                previousImage();
            } else {
                nextImage();
            }
        }, 200);

    }

    public void previousImage() {
        image1.setImageResource(imageArray[currentIndex]);

        currentIndex--;
        new Handler().postDelayed(() -> {
            if (currentIndex < startIndex) {
                currentIndex++;
                nextImage();
            } else {
                previousImage();
            }
        }, 200);

    }

    public void setLives() {
        switch (prefConfig.getWrongCount()) {
            case 0:
            case 1: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live);
                img3.setImageResource(R.drawable.live);
            }
            break;
            case 2: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live);
                img3.setImageResource(R.drawable.live_over);
            }
            break;
            case 3: {
                img1.setImageResource(R.drawable.live);
                img2.setImageResource(R.drawable.live_over);
                img3.setImageResource(R.drawable.live_over);
            }
            break;
            default: {
                img1.setImageResource(R.drawable.live_over);
                img2.setImageResource(R.drawable.live_over);
                img3.setImageResource(R.drawable.live_over);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TAG", "ON Resume");
        if (!isFinishing()) {
            guessedRightFlag = db.getGuessAnswer(song);
            if (guessedRightFlag == 1) {
                coin_add.setText("10");
            } else {
                coin_add.setText("50");
            }
            scoreCount = db.getScore(level);
            score.setText(String.valueOf(scoreCount));
            starButton.setVisibility(View.GONE);
            if (videoWatched) {
                Log.d("TAG", "ON Resume Video watch true");
                if (!currentSong.equals("")) {
                    prefConfig.clearWrongCount();
                    setLives();
                    db.setCorrect(currentSong, 0, 1, 0);
                }
                if (wrongAnswer != null && wrongAnswer.isShowing()) {
                    get_live_from = "video";
                    dialogScore1.setText(3 + " Lives");
                    txt_desc.setText("You earn " + 3 + " lives. Continue playing to earn more.");
                    lin_live_main.setVisibility(View.GONE);
                    img_incorrect.setVisibility(View.GONE);
                    title.setVisibility(View.GONE);
                    close.setVisibility(View.GONE);
                    rel_main.setVisibility(View.VISIBLE);
                }
            }

        }
        if (stopFlag) {
            stopFlag = false;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopMusic();
        stopFlag = true;
        play.setVisibility(View.VISIBLE);
        play.setClickable(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TAG", "onPause");
        stopFlag = false;
        stopMusic();
//        mRewardedVideoAd.destroy(QuizActivity.this);
        play.setVisibility(View.VISIBLE);
        pause.setVisibility(View.GONE);
    }

    private void loadRewardedVideoAd() {
        try {
            if (mRewardedVideoAd != null) {
                mRewardedVideoAd.loadAd(getResources().getString(R.string.rewared_video_ad),
                        new AdRequest.Builder().build());
            }
        } catch (Exception e) {
            Log.d("EXCEPTION:", "" + e);
        }
    }

    public void initView() {
        TIME_LEFT = COUNTDOWN_IN_MILLI;
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fading_star);
        left_to_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
        right_to_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);
        rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(COUNTDOWN_IN_MILLI);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        scoreCount = db.getScore(level);
        score.setText(String.valueOf(scoreCount));
        opt1.setVisibility(View.GONE);
        opt2.setVisibility(View.GONE);
        opt3.setVisibility(View.GONE);
        opt4.setVisibility(View.GONE);
    }

    public void initQuiz(int index) {
        int pos = index + 1;
        song = quizList.get(index).getSong_name();
        uri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + song);
        movie = quizList.get(index).getMovie_name();
        optionsList = db.getMovieOptions(movie);
        optionsList.add(movie);
        Collections.shuffle(optionsList);
        movieNameArray = optionsList.toArray(new String[optionsList.size()]);
        setButtonClickable(false);
        new Handler().postDelayed(() -> {
            if (!isFinishing()) {
                opt1.setVisibility(View.VISIBLE);
                opt2.setVisibility(View.VISIBLE);
                opt3.setVisibility(View.VISIBLE);
                opt4.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);
                pause.setVisibility(View.VISIBLE);

                quizNumber.setText("Quiz : " + pos + "/" + 60);
                opt1.setText(movieNameArray[0]);
                opt2.setText(movieNameArray[1]);
                opt3.setText(movieNameArray[2]);
                opt4.setText(movieNameArray[3]);
                startCountdown();
            }
        }, 1000);
    }

    private void startCountdown() {
        wrongCount = db.getWrongCount(song);
        currentSong = song;
        opt1.setVisibility(View.VISIBLE);
        opt2.setVisibility(View.VISIBLE);
        opt3.setVisibility(View.VISIBLE);
        opt4.setVisibility(View.VISIBLE);
        starButton.setVisibility(View.GONE);

        opt1.startAnimation(left_to_right);
        opt2.startAnimation(right_to_left);
        opt3.startAnimation(left_to_right);
        opt4.startAnimation(right_to_left);

        opt1.setBackgroundResource(R.drawable.round_button);
        opt2.setBackgroundResource(R.drawable.round_button);
        opt3.setBackgroundResource(R.drawable.round_button);
        opt4.setBackgroundResource(R.drawable.round_button);

        right_to_left.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                setButtonClickable(true);
                playMusic();
                pause.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

    }

    public void playMusic() {
        guessedRightFlag = db.getGuessAnswer(song);
        if (guessedRightFlag == 1) {
            progressBar.setVisibility(View.GONE);
            progresText.setVisibility(View.GONE);
            txt_live_msg.setVisibility(View.VISIBLE);
            coin_add.setText("10");

        } else {
            txt_live_msg.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            progresText.setVisibility(View.VISIBLE);
            coin_add.setText("50");

        }
//        txt_live_msg.setVisibility(View.GONE);
//        progresText.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.VISIBLE);
        countDownTimer = new CountDownTimer(TIME_LEFT, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                TIME_LEFT = millisUntilFinished;
                updateCountdownText();
                timerProgress.setProgress((int) TIME_LEFT);
                scoreCount = db.getScore(level);

//                    if (mediaPlayer == null && !isFinishing()) {
                if (mediaPlayer == null) {
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                        pause.setVisibility(View.VISIBLE);
                        play.setVisibility(View.GONE);
                        score.setText(String.valueOf(scoreCount));
                        mediaPlayer.setLooping(true);
                    }
                }
            }

            @Override
            public void onFinish() {
                TIME_LEFT = 0;
                updateCountdownText();
                timerProgress.setProgress((int) TIME_LEFT);
                stopMusic();

                Toast toast = Toast.makeText(getApplicationContext(), "Time Out!", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                pause.setVisibility(View.GONE);
//                play.setVisibility(View.VISIBLE);
                pause.setClickable(false);
                play.setClickable(false);
                setButtonClickable(false);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    TIME_LEFT = 20000;
                    if (position < 59) {
                        position = position + 1;
                        getData(position);
                        initQuiz(position);
                    } else {
                        showPopuplevelcomplete();
//                        finish();
                    }

                }, 2000);
            }
        }.start();
    }

    public void stopMusic() {
        stopTimer();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        StopPlay();
    }

    public void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    private void updateCountdownText() {
        int seconds = (int) ((TIME_LEFT) / 1000) % 60;
        String time_formatted = String.format(Locale.getDefault(), "%02d", seconds);
        timer.setText(time_formatted);
        if (TIME_LEFT < 10000) {
            guessedRightFlag = db.getGuessAnswer(song);
            if (guessedRightFlag == 1) {
                coin_add.setText("10");
            } else {
                coin_add.setText("25");
            }
            timer.setTextColor(Color.RED);
        } else {
            timer.setTextColor(Color.WHITE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mRewardedVideoAd.destroy(QuizActivity.this);
        stopMusic();
        if (mAdView!=null){
            Log.d("Adview", "Adview Destroy");
            mAdView.destroy();
        }
    }

    public void StopPlay() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            mediaCurrentPosition = mediaPlayer.getCurrentPosition();
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        pause.setVisibility(View.GONE);
        play.setVisibility(View.VISIBLE);
        setButtonClickable(false);
    }

    private void OptionSelected(Button answerButton) {
        stopMusic();
        pause.setClickable(false);
        play.setClickable(false);
        guessedRightFlag = db.getGuessAnswer(song);
        currentSong = song;
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (guessedRightFlag == 0) {
            if (answerButton.getText().equals(movie)) {
                answerButton.setBackgroundResource(R.drawable.right_selection);
                starButton.setVisibility(View.VISIBLE);
                if (TIME_LEFT < 10000) {
                    starButton.setText("25+");
                    prefConfig.setHighscore(25);
                    scoreCount += 25;
                } else {
                    starButton.setText("50+");
                    prefConfig.setHighscore(50);
                    scoreCount += 50;
                }

                db.setCorrect(song, 1, 1, 0);
                db.setScore(level, scoreCount);
                starButton.startAnimation(animation);
                score.setText(String.valueOf(scoreCount));
                nextQuiz();

            } else {
                answerButton.setBackgroundResource(R.drawable.wrong_selection);
                int wrongSelection = db.getWrongCount(song);
                wrongSelection++;
                db.setCorrect(song, 0, 1, wrongSelection);
                score.setText(String.valueOf(scoreCount));
//                if(prefConfig.getWrongCount()>3) {
                showWrongAnswerPopup();
//                }
            }
        } else {

            if (answerButton.getText().equals(movie)) {
                answerButton.setBackgroundResource(R.drawable.right_selection);
                starButton.setVisibility(View.VISIBLE);
                starButton.setText("10+");
                prefConfig.setHighscore(10);
                scoreCount += 10;
//                if (TIME_LEFT < 10000) {
//                    starButton.setText("25+");
//                    prefConfig.setHighscore(25);
//                    scoreCount += 25;
//                } else {
//                    starButton.setText("50+");
//                    prefConfig.setHighscore(50);
//                    scoreCount += 50;
//                }

                db.setCorrect(song, 1, 1, 0);
                db.setScore(level, scoreCount);
                starButton.startAnimation(animation);
                score.setText(String.valueOf(scoreCount));
                nextQuiz();

            } else {
                answerButton.setBackgroundResource(R.drawable.wrong_selection);
                nextQuiz();
//                int wrongSelection = db.getWrongCount(song);
//                wrongSelection++;
//                db.setCorrect(song, 0, 1, wrongSelection);
//                score.setText(String.valueOf(scoreCount));
//                if(prefConfig.getWrongCount()>3) {
//                showWrongAnswerPopup();
            }
//            else {
//                answerButton.setBackgroundResource(R.drawable.wrong_selection);
//                int wrongSelection = db.getWrongCount(song);
//                wrongSelection++;
//                db.setCorrect(song, 0, 1, wrongSelection);
//                score.setText(String.valueOf(scoreCount));
////                if(prefConfig.getWrongCount()>3) {
//                showWrongAnswerPopup();
////                }
//            }
//            Toast.makeText(QuizActivity.this, "Already Solved", Toast.LENGTH_SHORT).show();
//            nextQuiz();
        }

    }

    public void nextQuiz() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (wrongAnswer != null && wrongAnswer.isShowing()) {
                wrongAnswer.dismiss();
            }
            TIME_LEFT = COUNTDOWN_IN_MILLI;
            if (position >= 0 && position < 59) {
                position++;
                getData(position);
                initQuiz(position);
                opt1.setBackgroundResource(R.drawable.round_button);
                opt2.setBackgroundResource(R.drawable.round_button);
                opt3.setBackgroundResource(R.drawable.round_button);
                opt4.setBackgroundResource(R.drawable.round_button);

            } else {
                showPopuplevelcomplete();
//                    finish();
            }
        }, 1000);
    }

    private void getData(int position) {
        ArrayList<Questions> guessedWrong = db.getGuessedAnswerList(position + 1);
        int realProgress = db.getRightCount(level);
        int wrong_progress = db.getWrongofLevel(level);
        int progress = (realProgress + wrong_progress);
        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", 0, progress);
        progressAnimator.setDuration(1500);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.start();
        progressBar.setProgress(db.getRightCount(level));
        progressBar.setSecondaryProgress(db.getWrongofLevel(level));
        progresText.setText((position + 1) + " / 60");
    }

    public void showWrongAnswerPopup() {

        wrongAnswer = new Dialog(QuizActivity.this);
        wrongAnswer.setContentView(R.layout.wrong_answer_popup);
        dialogScore = wrongAnswer.findViewById(R.id.totalScore);
        previousScore = wrongAnswer.findViewById(R.id.previousScore);
        button = wrongAnswer.findViewById(R.id.shareOnFacebook);
        watchVideo = wrongAnswer.findViewById(R.id.watchVideo);
        lin_coins = wrongAnswer.findViewById(R.id.lin_coins);
        lin_share = wrongAnswer.findViewById(R.id.lin_share);
        close = wrongAnswer.findViewById(R.id.close);
        img_incorrect = wrongAnswer.findViewById(R.id.img_incorrect);
        attempts = wrongAnswer.findViewById(R.id.attempts);
        dialogScore1 = wrongAnswer.findViewById(R.id.totalScore1);
        Button button1 = wrongAnswer.findViewById(R.id.dismissbtn);
        txt_desc = wrongAnswer.findViewById(R.id.txt_desc);
        rel_main = wrongAnswer.findViewById(R.id.rel_main);

        title = wrongAnswer.findViewById(R.id.incorrectAnswer);
        lin_live_main = wrongAnswer.findViewById(R.id.lin_live_main);
        img_incorrect.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        close.setVisibility(View.VISIBLE);
        rel_main.setVisibility(View.GONE);
//        if (live_from_get.equals("facebook_login")) {
//            lin_live_main.setVisibility(View.GONE);
//            rel_main.setVisibility(View.VISIBLE);
//            dialogScore1.setText(3 + " Lives");
//            txt_desc.setText("You earn " + 3 + " lives. Continue playing to earn more.");
//        }else
//        {
        if (prefConfig.getWrongCount() <= 3) {
            lin_share.setVisibility(View.GONE);
            lin_live_main.setVisibility(View.GONE);
            attempts.setVisibility(View.VISIBLE);
            attempts.setText((3 - prefConfig.getWrongCount()) + " Attempts Remaining!");
            prefConfig.setWrongCount(1);
        }

//        dialogScore.setText(String.valueOf(totalscore));
        scoreCount = db.getScore(level);
        dialogScore.setText(String.valueOf(scoreCount));
        int totalscore = prefConfig.getHighscore();
        db.setScore(level, scoreCount);

        previousScore.setText(""+totalscore);
//        previousScore.setText(String.valueOf(totalscore));
//        prefConfig.setHighscore(prefConfig.getHighscore());

        button.setOnClickListener(this);
        watchVideo.setOnClickListener(this);
        memory = getAvailableRAM.getRamMemory(QuizActivity.this);
        Log.d("TAG", "RAM AVAILABLE : " + memory);
        if (memory < 400) {
            watchVideo.setClickable(false);
            watchVideo.setVisibility(View.GONE);
        } else {
            watchVideo.setClickable(true);
            watchVideo.setVisibility(View.VISIBLE);
        }

        if (!prefConfig.getFirstTimefblogin()) {
            button.setText("Login");
        }
//        }

        lin_coins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scoreCount = db.getScore(level);
                if (scoreCount < 500 || db.getScore(level) < 500) {
                    Toast.makeText(QuizActivity.this, "Insufficient coins", Toast.LENGTH_LONG).show();
                } else {  //if(prefConfig.getWrongCount() <=3  ) {

//                    wrongAnswer.dismiss();
//                    int coins = (prefConfig.getHighscore() - 500);
//                    prefConfig.setHighscoreDeduction(coins);
//                    prefConfig.clearWrongCount();

                    if (prefConfig.getWrongCount() <= 4 && prefConfig.getWrongCount() > 1) {
//                        setLives();
                        prefConfig.setWrongCountlive(prefConfig.getWrongCount() - 1);
                        scoreCount = db.getScore(level);
                        scoreCount = scoreCount - 500;
                        db.setScore(level, scoreCount);
                        starButton.startAnimation(animation);
                        score.setText(String.valueOf(scoreCount));
                        dialogScore1.setText(1 + " Lives");
                        txt_desc.setText("You earn " + 1 + " lives. Continue playing to earn more.");
                        lin_live_main.setVisibility(View.GONE);
                        img_incorrect.setVisibility(View.GONE);
                        title.setVisibility(View.GONE);
                        close.setVisibility(View.GONE);
                        rel_main.setVisibility(View.VISIBLE);
                        scoreCount = db.getScore(level);
                        dialogScore.setText(String.valueOf(scoreCount));
                        int totalscore = prefConfig.getHighscore();
                        db.setScore(level, scoreCount);
                        prefConfig.setHighscoreDeduction(prefConfig.getHighscore() - 500);
                        int total_new = prefConfig.getHighscore();
                        previousScore.setText("" + total_new);
//                        prefConfig.setHighscore(prefConfig.getHighscore());

//                        scoreCount += scoreCount;
//                        prefConfig.setHighscore(scoreCount);
                    } else {
                        Toast.makeText(QuizActivity.this, "You Already Have 3 lives.Continue playing", Toast.LENGTH_SHORT).show();
                    }
                }
//                else {
//                    wrongAnswer.dismiss();
//                    setLives();
//                    nextQuiz();
//                }

            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_live_from = "";
                lin_live_main.setVisibility(View.VISIBLE);
                img_incorrect.setVisibility(View.VISIBLE);
                title.setVisibility(View.VISIBLE);
                close.setVisibility(View.VISIBLE);
                rel_main.setVisibility(View.GONE);
//                if (wrongAnswer.isShowing() && wrongAnswer != null) {
//                    wrongAnswer.dismiss();
//                    setLives();
//                    nextQuiz();
//                }
            }
        });

        wrongAnswer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        if (!isFinishing()) {
        wrongAnswer.show();
//        }
        wrongAnswer.setCancelable(false);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrongAnswer.dismiss();
                if (get_live_from.equals("video")) {
                    TIME_LEFT = 20000;
                    startCountdown();
                } else if (get_live_from.equals("facebook")) {
                    mediaPlayer = MediaPlayer.create(QuizActivity.this, uri);
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                        pause.setVisibility(View.VISIBLE);
                        play.setVisibility(View.GONE);
                        score.setText(String.valueOf(scoreCount));
                        mediaPlayer.setLooping(true);
                        TIME_LEFT = 20000;
                        pause.setVisibility(View.VISIBLE);
                        play.setVisibility(View.GONE);
                        startCountdown();
                    }
                } else {
                    setLives();
                    nextQuiz();
                }
            }
        });
    }

    public void showCongratulationspopup(int live_count) {
        earn_dialog = new Dialog(QuizActivity.this);
        earn_dialog.setContentView(R.layout.earn_lives_dialog);
        dialogScore = earn_dialog.findViewById(R.id.totalScore1);
        button = earn_dialog.findViewById(R.id.dismissbtn);
        TextView txt_desc = earn_dialog.findViewById(R.id.txt_desc);
        dialogScore.setText(live_count + " Lives");
        txt_desc.setText("You earn " + live_count + " lives. Continue playing to earn more.");
        earn_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        if (!isFinishing()) {
        earn_dialog.show();
//        }
        earn_dialog.setCancelable(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                earn_dialog.dismiss();
                wrongAnswer.dismiss();
                setLives();
                nextQuiz();
            }
        });
    }

    public void watchVideo() {
        if (prefConfig.getWrongCount() == 1) {
            Toast.makeText(QuizActivity.this, "You Already have 3 lives. Continue playing.", Toast.LENGTH_SHORT).show();
        } else {
            memory = getAvailableRAM.getRamMemory(QuizActivity.this);
            if (checkInternet.isConnectedToInternet()) {
                try {
                    if (memory >= 400) {
                        if (mRewardedVideoAd != null && mRewardedVideoAd.isLoaded()) {
                            mRewardedVideoAd.show();
//                        prefConfig.setWrongCountlive(prefConfig.getWrongCount() + 1);
//                        prefConfig.setWrongCount(3);
                        } else {
                            if (!isFinishing()) {
                                Toast.makeText(QuizActivity.this, "Waiting for this ad to load. Please wait or click again...", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(QuizActivity.this, "Some Error Occurred", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(QuizActivity.this, "Some Error Occurred", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(QuizActivity.this, "Check Your internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setButtonClickable(Boolean clickable) {
        opt1.setClickable(clickable);
        opt2.setClickable(clickable);
        opt3.setClickable(clickable);
        opt4.setClickable(clickable);
    }

    public void showfullscreenad() {

        mInterstitialAd = new InterstitialAd(QuizActivity.this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_ad));
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                displayInterstitial();
            }

            @Override
            public void onAdClosed() {
                Log.d("TAG", "INTERSTITIAL AD CLOSED");
            }

            @Override
            public void onAdOpened() {
                Log.d("TAG", "INTERSTITIAL onAdOpened");
            }
        });
    }

    public void displayInterstitial() {
        if (mInterstitialAd.isLoaded())
            mInterstitialAd.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void showPopuplevelcomplete() {
        level_completed.setContentView(R.layout.completed_level_status_dialog);
        TextView total_score = level_completed.findViewById(R.id.totalScore);
        TextView incorrectAnswer = level_completed.findViewById(R.id.incorrectAnswer);
        TextView txt_yes = level_completed.findViewById(R.id.txt_yes);
        TextView txt_no = level_completed.findViewById(R.id.txt_no);
        TextView level_status = level_completed.findViewById(R.id.level_status);
        close = level_completed.findViewById(R.id.close);
        total_score.setText(String.valueOf(scoreCount));
        incorrectAnswer.setText("Level " + level + " completed");
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                level_completed.dismiss();
                finish();
            }
        });
        if (level == 7) {
            finish();
        } else {
            if ((55 - db.getRightCount(level)) == 0 || db.getRightCount(level) > 55) {
                txt_yes.setVisibility(View.VISIBLE);
                txt_no.setVisibility(View.VISIBLE);
                level_status.setText("Level " + (level + 1) + " is unlocked.. Do you want to play next level?");
            } else {
                txt_yes.setVisibility(View.GONE);
                txt_no.setVisibility(View.GONE);
                level_status.setText("Solve remaining " + (55 - db.getRightCount(level)) + " questions to unlock next level");
            }
        }
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                level_completed.dismiss();
                finish();
                startActivity(new Intent(QuizActivity.this, MainActivity.class));
                // prefConfig.setPagerPosition(level);
            }
        });
        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        level_completed.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        level_completed.setCancelable(false);
        level_completed.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    level_completed.dismiss();
                }
                return true;
            }
        });
        if (!QuizActivity.this.isFinishing())
            level_completed.show();
    }

    public void shareOnFb() {
        highscore = prefConfig.getHighscore();
        ShareLinkContent linkContent = new ShareLinkContent.Builder().
                setContentUrl(Uri.parse(getResources().getString(R.string.app_link)))
                .setQuote("Highscore = " + highscore)
                .build();

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(linkContent);
        }
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                if (!currentSong.equals("")) {
                    prefConfig.clearWrongCount();
                    setLives();
                    get_live_from = "video";
                    db.setCorrect(currentSong, 0, 1, 0);
                }
                if (wrongAnswer != null && wrongAnswer.isShowing()) {
                    if (prefConfig.getWrongCount() <= 3 && prefConfig.getWrongCount() >= 1) {
                        get_live_from = "video";
                        dialogScore1.setText(3 + " Lives");
                        txt_desc.setText("You earn " + 3 + " lives. Continue playing to earn more.");
                        lin_live_main.setVisibility(View.GONE);
                        img_incorrect.setVisibility(View.GONE);
                        title.setVisibility(View.GONE);
                        close.setVisibility(View.GONE);
                        rel_main.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(QuizActivity.this, "You Already Have 3 lives.Continue playing", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancel() {
                nextQuiz();
                Toast.makeText(QuizActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });
    }

    private void showPause() {
        pauseDialog = new Dialog(QuizActivity.this);
        pauseDialog.setContentView(R.layout.pause_game);
        resume = pauseDialog.findViewById(R.id.resume_game);
        back_game = pauseDialog.findViewById(R.id.back_game);
        exit = pauseDialog.findViewById(R.id.exit_game);
        resume.setOnClickListener(this);
        exit.setOnClickListener(this);
        back_game.setOnClickListener(this);
        pauseDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pauseDialog.show();
        pauseDialog.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.answer1:
                OptionSelected(opt1);
                break;

            case R.id.answer2:
                OptionSelected(opt2);
                break;

            case R.id.answer3:
                OptionSelected(opt3);
                break;

            case R.id.answer4:
                OptionSelected(opt4);
                break;

            case R.id.watchVideo:
//getwrongcount=4
                if ((prefConfig.getWrongCount() <= 4 && prefConfig.getWrongCount() > 0)) {
                    watchVideo();
//                   4-3=1

                } else {
                    Toast.makeText(QuizActivity.this, "You Already have 3 lives. Continue playing.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.shareOnFacebook:
                if ((prefConfig.getWrongCount() <= 4 && prefConfig.getWrongCount() > 0) || prefConfig.getWrongCount() != 1) {
                    shareOnFaceBook();
//                    if (checkInternet.isConnectedToInternet()) {
//                        if (!prefConfig.getFirstTimefblogin()) {
//                            Intent intent = new Intent(QuizActivity.this, FacebookLoginActivity.class);
//                            intent.putExtra("Position", position);
//                            intent.putExtra("Level", level);
//                            intent.putExtra("GuessedRight", guessedRightFlag);
//                            intent.putExtra("list", quizList);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                        } else {
//
//                            Toast.makeText(QuizActivity.this, "You Already have 3 lives. Continue playing.", Toast.LENGTH_SHORT).show();
//
//                        }
//                    } else {
//                        Toast.makeText(QuizActivity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(QuizActivity.this, "You Already have 3 lives. Continue playing.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.music_pause:
                StopPlay();
//                showfullscreenad();
                showPause();
                isPaused = true;
                break;

            case R.id.resume_game:
                Log.d("TAG", "RESUME");
                if (pauseDialog != null && pauseDialog.isShowing()) {
                    pauseDialog.dismiss();
                }
                if (mediaPlayer == null && !isFinishing()) {
                    mediaPlayer = MediaPlayer.create(QuizActivity.this, uri);
                    mediaPlayer.start();
                } else if (mediaPlayer != null && !mediaPlayer.isPlaying() && !isFinishing()) {
                    mediaPlayer.seekTo(mediaCurrentPosition);
                    mediaPlayer.start();
                }
                isPaused = false;
                startCountdown();
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);
                break;

            case R.id.exit_game:
                moveTaskToBack(true);
                finish();
                break;
            case R.id.back_game:
                finish();
                break;
            case R.id.music_player:
//                if (prefConfig.getWrongCount() > 3) {
//                    showWrongAnswerPopup();
//                } else {
                if (mediaPlayer == null && !isFinishing()) {
                    mediaPlayer = MediaPlayer.create(QuizActivity.this, uri);
                    mediaPlayer.start();
                } else if (mediaPlayer != null && !mediaPlayer.isPlaying() && !isFinishing()) {
                    mediaPlayer.seekTo(mediaCurrentPosition);
                    mediaPlayer.start();
                }
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);
                startCountdown();
//                }
                break;

        }
    }

    public void shareOnFaceBook() {
        if (prefConfig.getWrongCount() != 1) {
            if (checkInternet.isConnectedToInternet()) {
                if (!prefConfig.getFirstTimefblogin()) {
                    Intent intent = new Intent(QuizActivity.this, FacebookLoginActivity.class);
                    intent.putExtra("Position", position);
                    intent.putExtra("Level", level);
                    intent.putExtra("GuessedRight", guessedRightFlag);
                    intent.putExtra("list", quizList);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    shareOnFb();
                }
            } else {
                Toast.makeText(QuizActivity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(QuizActivity.this, "You Already have 3 lives. Continue playing.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.d("TAG", "onRewardedVideoAdLoaded");
        videoWatched = false;
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.d("TAG", "onRewardedVideoAdOpened");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.d("TAG", "onRewardedVideoStarted");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.d("TAG", "onRewardedVideoAdClosed");
        loadRewardedVideoAd();
        if (wrongAnswer != null && wrongAnswer.isShowing()) {
            wrongAnswer.dismiss();
        }
        nextQuiz();
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.d("TAG", "onRewarded");
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.d("REWARDEDAD", "onRewardedVideoAdLeftApplication");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.d("TAG", "onRewardedVideoAdFailedToLoad");
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoCompleted() {
        if (mRewardedVideoAd != null) {
            mRewardedVideoAd.destroy(QuizActivity.this);
        }
        Log.d("TAG", "RewardedVideoCompleted");
        videoWatched = true;
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(QuizActivity.this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();
    }

}
