package com.client.techathalon.bollywoodsongquiz;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.client.techathalon.bollywoodsongquiz.Adapters.QuestionsAdapter;
import com.client.techathalon.bollywoodsongquiz.DatabaseOperations.DatabaseHelper;
import com.client.techathalon.bollywoodsongquiz.ModelClass.CustomModel;
import com.client.techathalon.bollywoodsongquiz.ModelClass.Questions;
import com.facebook.CallbackManager;
import com.facebook.share.widget.ShareDialog;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

public class QuizQuestions extends AppCompatActivity {

    RecyclerView recyclerView;
    DatabaseHelper db;
    ArrayList<Questions> list;
    public QuestionsAdapter adapter;
    int level;
    TextView level_name;
    GridLayoutManager gridLayoutManager;
    CallbackManager callbackManager;
    ArrayList<Questions> wrongAnsweredList;
    ShareDialog shareDialog;
    SharedPrefConfig prefConfig;
    Dialog level_completed, wrong_answers;
    TextView total_score, level_score;
    ProgressDialog progressDialog;
    ImageView close;
    ArrayList<Integer> guessed;
    ArrayList<Integer> isPlayed;
    Sprite doubleBounce;
    SpinKitView spinKitView;
    GetAllData getAllData;
    RelativeLayout transparentView;
    Questions questions;
    int guessedRight, guessedWrong, totalscore;
    SharedPreferences sharedPreferences;
    CheckInternet checkInternet;
    RelativeLayout layout;
    String from_where = "";
    private FirebaseAnalytics mFirebaseAnalytics;
    // CrashlyticsCore core;
    ProgressBar progressBar;
    GetAvailableRAM getAvailableRAM;
    AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_questions);

//        FirebaseApp.initializeApp(getApplicationContext());
//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder()
//                        .disabled(BuildConfig.DEBUG)
//                        .build())
//                .build();
//        Fabric.with(this, crashlyticsKit);
//        // Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//
//        Crashlytics.setString("ACTIVITY_NAME", "QuizQuestions");
//        Crashlytics.log("QuizQuestions");

        mAdView = findViewById(R.id.adView);
        getAvailableRAM = new GetAvailableRAM();
        checkInternet = new CheckInternet();
        progressBar = findViewById(R.id.quiz_progress_bar);
        progressBar.setVisibility(View.GONE);
        if (checkInternet.isConnectedToInternet()) {
            mAdView.setVisibility(View.VISIBLE);
            MobileAds.initialize(this, new OnInitializationCompleteListener() {
                @Override
                public void onInitializationComplete(InitializationStatus initializationStatus) {
                }
            });
//            mAdView = findViewById(R.id.ads);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        } else {
            mAdView.setVisibility(View.GONE);
        }
        layout = findViewById(R.id.quiz_question_layout);
        level_name = findViewById(R.id.level_number);
        shareDialog = new ShareDialog(this);
        level_score = findViewById(R.id.level_score);

        Intent intent = getIntent();
        level = intent.getIntExtra("LevelCount", 1);
        from_where = intent.getStringExtra("from");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            switch (level) {
                case 1:
                    layout.setBackground(getDrawable(R.drawable.gradient_blue));
                    break;
                case 2:
                    layout.setBackground(getDrawable(R.drawable.gradient_pink));
                    break;
                case 3:
                    layout.setBackground(getDrawable(R.drawable.gradient_green));
                    break;
                case 4:
                    layout.setBackground(getDrawable(R.drawable.gradient_purple));
                    break;
                case 5:
                    layout.setBackground(getDrawable(R.drawable.gradient_red));
                    break;
                case 6:
                    layout.setBackground(getDrawable(R.drawable.gradient_darkpurple));
                    break;
                case 7:
                    layout.setBackground(getDrawable(R.drawable.gradient_orange));
                    break;
            }
        }

        db = new DatabaseHelper(getApplicationContext());
        sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.level_locked), MODE_PRIVATE);
        guessedRight = 0;
        guessedWrong = 0;

        level_name.setText("Level " + level);
        level_score.setText(String.valueOf(db.getScore(level)));
        isPlayed = new ArrayList<>();

        level_completed = new Dialog(QuizQuestions.this);
        wrong_answers = new Dialog(QuizQuestions.this);
        guessed = new ArrayList<>();
        spinKitView = findViewById(R.id.spin_kit);
        transparentView = findViewById(R.id.transparentView);
        list = new ArrayList<>();
        prefConfig = new SharedPrefConfig(getApplicationContext());
        prefConfig.clearTabPreference();
        adapter = new QuestionsAdapter(getApplicationContext(), list, wrongAnsweredList);
        progressDialog = new ProgressDialog(this);
        doubleBounce = new DoubleBounce();

        recyclerView = findViewById(R.id.recyclerView);
        callbackManager = CallbackManager.Factory.create();

//        adapter = new QuestionsAdapter(getApplicationContext(), list, wrongAnsweredList);
        getAllData = new GetAllData();
        getAllData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        totalscore = prefConfig.getHighscore();
    }



    public static class Utility {
        static int calculateNoOfColumns(Context context) {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
            return (int) (dpWidth / 120);
        }
    }

    public class GetAllData extends AsyncTask<Void, Integer, Void> {

        int id;
        String song_name;
        String movie_name;
        int is_played;
        int scores;
        int level_count;
        int guessed_right;
        int level_unlocked;
        int wrongCount;
        Cursor cursor;

        @Override
        protected void onPreExecute() {
            list = new ArrayList<>();
            guessedRight = 0;
            guessedWrong = 0;
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
//            Crashlytics.setString("ACTIVITY_NAME", "QuizQuestions : GetAllData ");
//            Crashlytics.log("QuizQuestions : GetAllData");
            cursor = db.getSongRecords(level);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        id = cursor.getInt(0);
                        song_name = cursor.getString(1);
                        movie_name = cursor.getString(2);
                        is_played = cursor.getInt(3);
                        scores = cursor.getInt(4);
                        level_count = cursor.getInt(5);
                        guessed_right = cursor.getInt(6);
                        level_unlocked = cursor.getInt(7);
                        wrongCount = cursor.getInt(8);
                        if (is_played == 1) {
                            if (guessed_right == 1) {
                                guessed.add(id);
                                guessedRight++;
                            } else {
                                guessedWrong++;
                            }
                        }
                        questions = new Questions(id, song_name, movie_name, is_played, level_count,
                                guessed_right, level_unlocked, wrongCount);
                        list.add(questions);

                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                Log.d("Exception", "Exception = " + e.toString());
//                Crashlytics.logException(e);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progressBar.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (cursor != null) {
                cursor.close();
            }
            if (list.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Something went wrong! Try Again", Toast.LENGTH_SHORT).show();
            }
            progressBar.setVisibility(View.GONE);
            adapter = new QuestionsAdapter(getApplicationContext(), list, wrongAnsweredList);
            int mNoOfColumns = QuizQuestions.Utility.calculateNoOfColumns(getApplicationContext());
            gridLayoutManager = new GridLayoutManager(getApplicationContext(), mNoOfColumns);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);

            if (guessedRight >= 55) {
                if (level != 6) {
                    db.unlockLevel(level + 1);
                    prefConfig.writeLevelLocked(level);
                }
            }
            if (guessedRight == 60) {
                if (!prefConfig.readShareOnFb(level)) {
                    showPopup();
                }
            }
            if (guessedWrong >= 25) {
                if (!prefConfig.readWrongShareOnFb(level)) {
                    showWrongAnswerPopup();
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        transparentView.setVisibility(View.VISIBLE);
//        spinKitView.setVisibility(View.VISIBLE);
        progressDialog.setIndeterminateDrawable(doubleBounce);

        getAllData = new GetAllData();
        db = new DatabaseHelper(getApplicationContext());
        getAllData.execute();
        adapter.notifyDataSetChanged();
        level_score.setText(String.valueOf(db.getScore(level)));
        Log.d("TAG", "ONRESTART");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void showPopup() {
        level_completed.setContentView(R.layout.completed_level);
        total_score = level_completed.findViewById(R.id.totalScore);
        Button button = level_completed.findViewById(R.id.shareFacebook);
        close = level_completed.findViewById(R.id.close);
        total_score.setText(String.valueOf(totalscore));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                level_completed.dismiss();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ShareOnFb(totalscore, level_completed, getApplicationContext(), shareDialog, callbackManager, level, "LevelCompletion");
            }
        });
        level_completed.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        level_completed.setCancelable(false);
        level_completed.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    level_completed.dismiss();
                }
                return true;
            }
        });
        if (!QuizQuestions.this.isFinishing())
            level_completed.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wrong_answers != null && wrong_answers.isShowing()) {
            wrong_answers.dismiss();
        }
        if (mAdView!=null){
            Log.d("Adview", "Adview Destroy");
            mAdView.destroy();
        }
//        if (db != null) {
//            db.close();
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (wrong_answers != null) {
            wrong_answers.dismiss();
        }
    }

    public void showWrongAnswerPopup() {
//        LayoutInflater layoutInflater = getLayoutInflater();
//        View customView = layoutInflater.inflate(R.layout.wrong_answer_popup, null);

        wrong_answers = new Dialog(QuizQuestions.this);
        wrong_answers.setContentView(R.layout.wrong_answers_share);

        total_score = wrong_answers.findViewById(R.id.totalScore);
        Button button = wrong_answers.findViewById(R.id.shareFacebook2);
//        close = wrong_answers.findViewById(R.id.close);
//        close.setVisibility(View.GONE);
        int total_new = prefConfig.getHighscore();
        total_score.setText("" + total_new);
//        total_score.setText(String.valueOf(totalscore));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ShareOnFb(total_new, wrong_answers, getApplicationContext(), shareDialog, callbackManager, level, "WrongAnswers");
            }
        });

        wrong_answers.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        wrong_answers.setCancelable(false);
        if (!isFinishing()) {
            wrong_answers.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            wrong_answers.show();
            wrong_answers.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        wrong_answers.dismiss();
                        Intent i = new Intent(QuizQuestions.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                    return true;
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (from_where != null && from_where.equalsIgnoreCase("fromfb")) {
            startActivity(new Intent(QuizQuestions.this, MainActivity.class));
            finish();
        } else {
            CustomModel.getInstance().changeState(list);
        }
    }
}
