package com.client.techathalon.bollywoodsongquiz;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;


public class ShareOnFb {

    int score;
    Dialog popup;
    Context context;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    SharedPrefConfig sharedPrefConfig;
    Integer level;
    String type;

    CheckInternet checkInternet = new CheckInternet();


    public ShareOnFb(int score, Dialog popup, Context context,
                     ShareDialog shareDialog, CallbackManager callbackManager,
                     int level, String type) {
        this.score = score;
        this.popup = popup;
        this.context = context;
        this.shareDialog = shareDialog;
        this.callbackManager = callbackManager;
        this.level = level;
        this.type = type;

        if (checkInternet.isConnectedToInternet()) {
            shareOnFb();
        }else {
            Toast.makeText(context, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void shareOnFb() {
        sharedPrefConfig = new SharedPrefConfig(context);
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentUrl(Uri.
                        parse("https://play.google.com/store/apps/details?id=com.client.techathalon.bollywoodsongquiz"))
                .setQuote("Highscore = " + score)
                .build();

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(linkContent);
        }

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                popup.dismiss();
                if (type.equals("LevelCompletion")) {
                    sharedPrefConfig.writeShareOnFb(true, level);
                } else if (type.equals("WrongAnswers")) {
                    sharedPrefConfig.writeWrongShareOnFb(true, level);
                }
            }

            @Override
            public void onCancel() {
                Toast.makeText(context, "Cancel", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });
    }

}
