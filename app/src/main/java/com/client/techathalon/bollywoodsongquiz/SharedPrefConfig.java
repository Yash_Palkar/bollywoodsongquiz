package com.client.techathalon.bollywoodsongquiz;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

//import com.crashlytics.android.Crashlytics;

public class SharedPrefConfig {

    private SharedPreferences sharedPreferences;
    private Context context;
    SharedPreferences.Editor editor;
    ArrayList<Integer> levelLockList = new ArrayList<>();
    ArrayList<String> songsPlayed = new ArrayList<>();
    private FirebaseAnalytics mFirebaseAnalytics;

    public SharedPrefConfig(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.first_installed),
                Context.MODE_PRIVATE);
    }

    public void clearPreference() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void clearTabPreference() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(context.getResources().getString(R.string.tab_number));
        editor.apply();
    }
 public void clearWrongCount() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(context.getResources().getString(R.string.wrong_count));
        editor.apply();
    }

    public void writeFirstInstallStatus(boolean status) {
        levelLockList.add(0, 1);
        levelLockList.add(1, 0);
        levelLockList.add(2, 0);
        levelLockList.add(3, 0);
        levelLockList.add(4, 0);
        levelLockList.add(5, 0);
        levelLockList.add(6, 0);
        editor = sharedPreferences.edit();
        for (int i = 0; i < levelLockList.size(); i++) {
            editor.putInt("KeyOFList" + i, levelLockList.get(i));
        }
        editor.putBoolean(context.getResources().getString(R.string.first_installed), status);
        editor.apply();
    }

    public boolean readFirstInstallStatus() {
        boolean status = true;
        status = sharedPreferences.getBoolean(context.getResources().getString(R.string.first_installed), true);
        return status;
    }

    public void setHighscore(int score) {
        int highscore = getHighscore() + score;
        editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.total_score), highscore);
        editor.apply();
    }
    public void setHighscoreDeduction(int score) {
//        int highscore = getHighscore() + score;
        editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.total_score), score);
        editor.apply();
    }

    public int getHighscore() {
        int highscore = 0;
        highscore = sharedPreferences.getInt(context.getResources().getString(R.string.total_score), 0);
        return highscore;
    }
    public void setWrongCount(int wrongcount) {
        int wrongCount = getWrongCount()+ wrongcount;
        editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.wrong_count), wrongCount);
        editor.apply();
    }

    public void setWrongCountlive(int wrongCount)
    {
        editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.wrong_count), wrongCount);
        editor.apply();
    }
    public int getWrongCount() {
        int wrongcount = 1;
        wrongcount = sharedPreferences.getInt(context.getResources().getString(R.string.wrong_count), 1);
        return wrongcount;
    }

    public void setTabNumber(int tab) {
        editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.tab_number), tab);
        editor.apply();
    }

    public int getTabNumber() {
        int tab = 0;
        tab = sharedPreferences.getInt(context.getResources().getString(R.string.tab_number), 0);
        return tab;
    }

    public void setFirstTimefblogin(boolean is_fb_login) {
        editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.fb_login), is_fb_login);
        editor.apply();
    }

    public boolean getFirstTimefblogin() {
        return sharedPreferences.getBoolean(context.getResources().getString(R.string.fb_login), false);
    }

    public void writeLevelLocked(int level) {
        List<Integer> newList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            newList.add(sharedPreferences.getInt("KeyOFList" + i, 0));
        }
        levelLockList.addAll(newList);
        levelLockList.set(level, 1);
        editor = sharedPreferences.edit();
        editor.putInt("KeyOFList" + level, levelLockList.get(level));
        editor.apply();
    }

    public int readLevelLocked(int level) {
        int level_locked = 0;
        List<Integer> newList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            newList.add(sharedPreferences.getInt("KeyOFList" + i, 0));
        }
        level_locked = newList.get(level);
        return level_locked;
    }

    void writeShareOnFb(boolean status, int level) {
        editor = sharedPreferences.edit();
        editor.putBoolean("shared_on_fb" + level, status);
        editor.apply();
    }

    boolean readShareOnFb(int level) {
        boolean status = true;
        status = sharedPreferences.getBoolean("shared_on_fb" + level, false);
        return status;
    }

    void writeWrongShareOnFb(boolean status, int level) {
        editor = sharedPreferences.edit();
        editor.putBoolean("wrong_shared_on_fb" + level, status);
        editor.apply();
    }

    public boolean readWrongShareOnFb(int level) {
        boolean status = true;
        status = sharedPreferences.getBoolean("wrong_shared_on_fb" + level, false);
        return status;
    }

    public void setPagerPosition(int position) {
        editor = sharedPreferences.edit();
        editor.putInt("pager_position", position);
        editor.commit();
    }

    public int getPagerPosition() {
        int position = 0;
        position = sharedPreferences.getInt("pager_position", 0);
        return position;
    }
}
