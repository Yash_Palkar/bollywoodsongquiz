package com.client.techathalon.bollywoodsongquiz;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.client.techathalon.bollywoodsongquiz.DatabaseOperations.DatabaseHelper;
import com.client.techathalon.bollywoodsongquiz.HttpHelperClass.AlarmReceiver;
import com.google.firebase.FirebaseApp;

import java.util.Calendar;

public class Splash extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    SharedPrefConfig prefConfig;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FirebaseApp.initializeApp(this.getApplicationContext());
        text = findViewById(R.id.app_name);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        prefConfig = new SharedPrefConfig(getApplicationContext());

        if (prefConfig.readFirstInstallStatus()) {
            Intent alarmIntent = new Intent(this, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 15);
            calendar.set(Calendar.MINUTE, 30);
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            databaseHelper.deleteSongs();
            databaseHelper.initDatabaseData();
        }

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_from_bottom);
        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade);
        text.startAnimation(animation);
        text.startAnimation(animation1);

        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(Splash.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 2000);

        } catch (Exception e) {
            Log.d("TAG", "Exception occurred : " + e);
            startActivity(new Intent(Splash.this, MainActivity.class));
            finish();
        }

    }

    @Override
    public void onBackPressed() {
    }
}

