package com.client.techathalon.bollywoodsongquiz;

public interface WSCallerVersionListener {
    void onGetResponse(boolean isUpdateAvailable);
}
